/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli

import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import de.denic.aim.client.AgentId
import spock.lang.Specification

import static java.io.File.createTempFile
import static java.lang.System.lineSeparator
import static java.util.Collections.singleton
import static java.util.UUID.randomUUID

class AgentConfigSpec extends Specification {

  AgentId AGENT_ID = new AgentId(randomUUID().toString())
  URI AIM_SERVER_URI = URI.create('http://test.aim.server.uri')
  Collection<RSAKey> SINGLE_KEY = singleton(new RSAKeyGenerator(2048).keyID(randomUUID().toString()).generate())

  def 'Serializing to JSON and deserializing from there again'() {
    given:
    def CUT = new AgentConfig(AGENT_ID, AIM_SERVER_URI, SINGLE_KEY)
    def targetOutputStream = new ByteArrayOutputStream()
    def sourceInputStream

    when:
    CUT.savedTo(targetOutputStream)
    def bytesWritten = targetOutputStream.toByteArray()
    println "JSON serialized Config:${lineSeparator()}${new String(bytesWritten, 'UTF-8')}"
    sourceInputStream = new ByteArrayInputStream(bytesWritten)

    then:
    CUT == AgentConfig.loadFrom(sourceInputStream)
  }

  def 'Changing instance fields are reflected in backing file'() {
    given:
    def targetFile = createTempFile('agent-config', '.json')
    def CUT = new AgentConfig(AGENT_ID, AIM_SERVER_URI, SINGLE_KEY).savedTo(targetFile)

    when:
    def modifiedCUT = CUT.modify().withTargetAimServerURI(URI.create('http://other.aim.server')).finished()
    def reloadedCUT = AgentConfig.loadFromFile(targetFile).get()

    then:
    modifiedCUT == reloadedCUT

    cleanup:
    targetFile.deleteOnExit()
  }

}
