/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

class Id4MeIdentifierSpec extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

  def 'Proper JSON serialization/deserialization'() {
    setup:
    def CUT = new Id4Me('id.4me.identifier')

    when: 'Serializing to JSON'
    def json = OBJECT_MAPPER.writerFor(Id4Me).writeValueAsString(CUT)

    then:
    json == '"id.4me.identifier"'

    when: 'Deserializing again'
    def deserialized = OBJECT_MAPPER.readerFor(Id4Me).readValue(json)

    then:
    deserialized == CUT
  }

}
