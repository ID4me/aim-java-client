/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import de.denic.aim.client.AgentData;
import de.denic.aim.client.AgentId;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.business.impl.AgentImpl;
import de.denic.aim.client.http.AimClient;
import de.denic.aim.client.http.AimClientException;
import de.denic.aim.client.http.impl.AimClientMicronautBasedImpl;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.RxHttpClient;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public interface Agent extends AutoCloseable {

  /**
   * @see AimClient#issueAuthorisationWith(Id4Me, Locale)
   */
  @Nonnull
  IdentityAuthorisation issueAuthorisationWith(@Nonnull Id4Me id4Me,
                                               @Nonnull Locale locale) throws AimClientException;

  @NotThreadSafe
  final class Builder {

    @Nonnull
    public static NewlyRegisteringBuilder toRegisterAt(@Nonnull final URI aimServerURI) {
      return new NewlyRegisteringBuilderImpl(aimServerURI);
    }

    @Nonnull
    public static AlreadyRegisteredBuilder alreadyRegisteredAt(@Nonnull final URI aimServerURI) {
      return new AlreadyRegisteredBuilderImpl(aimServerURI);
    }

    public interface NewlyRegisteringBuilder {

      @Nonnull
      NameSettingBuilder withNewlyGeneratedRSAKeyPairOfSize(int size) throws IllegalArgumentException;

      /**
       * @param keyPair Required to has Public/Private Key pair set ({@link RSAKey#toPublicKey()}/
       *                {@link RSAKey#toPrivateKey()}).
       */
      @Nonnull
      NameSettingBuilder withGiven(@Nonnull RSAKey keyPair);

    }

    public interface AlreadyRegisteredBuilder {

      /**
       * @see AgentImpl#AgentImpl(URL, JWK)
       */
      @Nonnull
      Agent withKeyPair(@Nonnull JWK keyPair);

    }

    public interface NameSettingBuilder {

      @Nonnull
      FullNameSettingBuilder andName(@NotEmpty String name);

    }

    public interface FullNameSettingBuilder {

      @Nonnull
      EmailSettingBuilder andFullName(@NotEmpty String fullName);

    }

    public interface EmailSettingBuilder {

      @Nonnull
      IssuerUriSettingBuilder andEmail(@Nonnull @Email String email);

    }

    public interface IssuerUriSettingBuilder {

      @Nonnull
      CallbackSettingBuilder andIssuerURI(@Nonnull URI issuerURI);

    }

    public interface CallbackSettingBuilder {

      /**
       * @throws AimClientException If creation of Agent fails.
       */
      @Nonnull
      Agent andCallbackTo(@Nonnull AgentRegisteringCallback creationCallback) throws AimClientException;

    }

    public interface AgentRegisteringCallback {

      /**
       * @param agentId {@link AgentId} of the newly registered {@link Agent}.
       * @param status  Optional {@link AgentData#getStatus()} field reported by AIM server's response.
       * @param keyPair Created ({@link NewlyRegisteringBuilder#withNewlyGeneratedRSAKeyPairOfSize(int)})
       *                or provided ({@link NewlyRegisteringBuilder#withGiven(RSAKey)}) key pair
       *                <strong>enriched</strong> with the {@link AgentId} as
       *                {@link RSAKey#getKeyID()} ... to get saved for later usage, e.g. via
       *                {@link Agent.Builder.NewlyRegisteringBuilder#withGiven(RSAKey)}!
       */
      void agentRegisteredWith(@Nonnull AgentId agentId, @Nonnull Optional<String> status, @Nonnull RSAKey keyPair);

    }

    private static class NewlyRegisteringBuilderImpl implements NewlyRegisteringBuilder, NameSettingBuilder,
            FullNameSettingBuilder, EmailSettingBuilder, IssuerUriSettingBuilder, CallbackSettingBuilder {

      private static final Logger LOG = LoggerFactory.getLogger(NewlyRegisteringBuilderImpl.class);

      private final URL aimServerURL;
      private RSAKey keyPair;
      private String name;
      private String fullName;
      private String email;
      private URI issuerURI;

      private NewlyRegisteringBuilderImpl(@Nonnull final URI aimServerURI) {
        try {
          this.aimServerURL = Validate.notNull(aimServerURI, "Missing AIM server URI").toURL();
        } catch (final MalformedURLException e) {
          throw new IllegalArgumentException(e);
        }
      }

      @Nonnull
      @Override
      public NameSettingBuilder withNewlyGeneratedRSAKeyPairOfSize(final int size) throws IllegalArgumentException {
        try {
          this.keyPair = new RSAKeyGenerator(size).generate();
        } catch (final JOSEException e) {
          throw new IllegalArgumentException("Generation of RSA key pair failed.", e);
        }

        return this;
      }

      @Nonnull
      @Override
      public NameSettingBuilder withGiven(@Nonnull final RSAKey keyPair) {
        this.keyPair = Validate.notNull(keyPair, "Missing RSA key pair");
        try {
          Validate.notNull(keyPair.toPublicKey(), "Missing Public Key of key pair");
          Validate.notNull(keyPair.toPrivateKey(), "Missing Private Key of key pair");
        } catch (JOSEException e) {
          throw new IllegalArgumentException("Accessing key failed", e);
        }

        return this;
      }

      @Nonnull
      @Override
      public FullNameSettingBuilder andName(@NotEmpty final String name) {
        this.name = Validate.notEmpty(name, "Missing name");
        return this;
      }

      @Nonnull
      @Override
      public EmailSettingBuilder andFullName(@NotEmpty final String fullName) {
        this.fullName = Validate.notEmpty(fullName, "Missing full name");
        return this;
      }

      @Nonnull
      @Override
      public IssuerUriSettingBuilder andEmail(@Nonnull @Email final String email) {
        this.email = Validate.notEmpty(email, "Missing email");
        return this;
      }

      @Nonnull
      @Override
      public CallbackSettingBuilder andIssuerURI(@Nonnull final URI issuerURI) {
        this.issuerURI = Validate.notNull(issuerURI, "Missing issuer URI");
        return this;
      }

      @Nonnull
      @Override
      public Agent andCallbackTo(@Nonnull final AgentRegisteringCallback creationCallback) throws AimClientException {
        Validate.notNull(creationCallback, "Missing callback instance");
        final AgentData agentData;
        try (final HttpClient httpClient = RxHttpClient.create(aimServerURL)) {
          agentData = new AimClientMicronautBasedImpl(aimServerURL, keyPair, httpClient)
                  .registerAgentWith(name, fullName, email, issuerURI);
        }
        final Map<String, Object> jsonFieldsOfKeyPair = keyPair.toJSONObject();
        jsonFieldsOfKeyPair.put("kid", agentData.getId().getValue());
        final RSAKey idEnrichedKeyPair;
        try {
          idEnrichedKeyPair = RSAKey.parse(jsonFieldsOfKeyPair);
        } catch (final ParseException e) {
          throw new RuntimeException("Internal error", e);
        }

        try {
          creationCallback.agentRegisteredWith(agentData.getId(), agentData.getStatus(), idEnrichedKeyPair);
        } catch (final RuntimeException e) {
          LOG.warn("Invocation of callback method on '{}' raises: ", creationCallback, e);
        }
        return new AgentImpl(aimServerURL, idEnrichedKeyPair);
      }

    }

    private static class AlreadyRegisteredBuilderImpl implements AlreadyRegisteredBuilder {

      private final URL aimServerURL;

      private AlreadyRegisteredBuilderImpl(@Nonnull final URI aimServerURI) {
        try {
          this.aimServerURL = Validate.notNull(aimServerURI, "Missing AIM server URI").toURL();
        } catch (final MalformedURLException e) {
          throw new IllegalArgumentException("AIM server URL '" + aimServerURI + "' " + e.getMessage());
        }
      }

      @Nonnull
      @Override
      public Agent withKeyPair(@Nonnull final JWK keyPair) {
        return new AgentImpl(aimServerURL, keyPair);
      }

    }

  }

}
