/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business.impl;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.dns.Zone;
import com.google.cloud.dns.*;
import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.business.DnsSetupException;
import de.denic.aim.client.business.DnsSetupSPI;
import de.denic.aim.client.business.Identity;
import de.denic.aim.client.business.IdentityAuthorisation.DnsSetup;
import de.denic.aim.client.http.Challenge;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.Optional;

import static com.google.cloud.dns.Dns.RecordSetListOption.dnsName;
import static java.lang.Thread.currentThread;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.stream.StreamSupport.stream;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.xbill.DNS.DClass.IN;
import static org.xbill.DNS.Message.newQuery;
import static org.xbill.DNS.Rcode.NOERROR;

@ThreadSafe
public final class DnsSetupViaGoogleDnsCloudAPI implements DnsSetupSPI {

  public static final int SPI_PRIORITY = 2 * DnsSetupViaGCloudCommand.SPI_PRIORITY;
  public static final String GOOGLE_DNS_API_KEY_FILE_NAME_KEY = "GOOGLE_DNS_API_KEY_FILE";
  public static final String DEFAULT_GOOGLE_DNS_API_KEY_FILE_NAME = "./google-dns-api-key.json";

  private static final Logger LOG = LoggerFactory.getLogger(DnsSetupViaGoogleDnsCloudAPI.class);
  private static final Duration ONE_SECOND = Duration.ofSeconds(1L);
  private static final Duration TEN_SECONDS = ONE_SECOND.multipliedBy(10L);
  private static final Duration ASSUMED_DNS_QUERY_LATENCY = Duration.ofMillis(50L);
  private static final Name ACME_CHALLENGE = Name.fromConstantString("_acme-challenge");
  private static final String ACME_CHALLENGE_PREFIX = ACME_CHALLENGE.toString() + ".";
  private static final boolean NOT_IN_PARALLEL = false;

  private final Object goodleCloudDnsGuard = new Object();
  @GuardedBy("goodleCloudDnsGuard")
  private Dns googleCloudDns;
  private final ServiceAccountCredentials accountCredentials;

  public DnsSetupViaGoogleDnsCloudAPI() {
    ServiceAccountCredentials tmpAccountCredentials;
    final String googleDnsApiKeyFileName = loadSystemOrEnvironmentProperty(GOOGLE_DNS_API_KEY_FILE_NAME_KEY,
            DEFAULT_GOOGLE_DNS_API_KEY_FILE_NAME);
    try (final InputStream credentialsStream = new FileInputStream(googleDnsApiKeyFileName)) {
      tmpAccountCredentials = ServiceAccountCredentials.fromStream(credentialsStream);
    } catch (final IOException e) {
      LOG.warn("Setup Google API Authorisation credentials from '{}' failed", googleDnsApiKeyFileName, e);
      tmpAccountCredentials = null;
    }
    this.accountCredentials = tmpAccountCredentials;
  }

  private static String loadSystemOrEnvironmentProperty(final String key, final String defaultValue) {
    final String envValue = System.getenv(key);
    if (!isEmpty(envValue))
      return envValue;

    return System.getProperty(key, defaultValue);
  }

  @Override
  public int getPriority() {
    return SPI_PRIORITY;
  }

  @Override
  public boolean accountableFor(@Nonnull final Challenge challenge) {
    if (accountCredentials == null)
      return false;

    final String identifierFQDN = Validate.notNull(challenge, "Missing ACME challenge").getIdentifier().toFullyQualifiedDnsName();
    setupGoogleCloudDnsIfNecessary();
    final Page<Zone> zoneList;
    synchronized (goodleCloudDnsGuard) {
      zoneList = googleCloudDns.listZones();
    }
    return stream(zoneList.iterateAll().spliterator(), NOT_IN_PARALLEL)
            .map(ZoneInfo::getDnsName)
            .anyMatch(identifierFQDN::endsWith);
  }

  private void setupGoogleCloudDnsIfNecessary() {
    synchronized (goodleCloudDnsGuard) {
      if (googleCloudDns == null) {
        googleCloudDns = DnsOptions.newBuilder().setCredentials(accountCredentials).build().getService();
      }
    }
  }

  @Nonnull
  @Override
  public DnsSetup setup(@Nonnull final Challenge challenge,
                        @Nonnull final JWK keyPair,
                        @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) throws DnsSetupException {
    final Id4Me id4Me = Validate.notNull(challenge, "Missing Challenge").getIdentifier();
    final String acmeChallengeRecordData = challenge.computeACMEChallengeValueWith(keyPair);
    final Zone googleCloudZone = findGoogleCloudManagedZoneMatching(id4Me);
    final RecordSet acmeChallengeRecord = RecordSet
            .newBuilder(ACME_CHALLENGE_PREFIX + id4Me.toFullyQualifiedDnsName(), RecordSet.Type.TXT)
            .setTtl(5, MINUTES)
            .addRecord("\"" + acmeChallengeRecordData + "\"")
            .build();
    final ChangeRequestInfo changeRequestInfo = ChangeRequestInfo.newBuilder().add(acmeChallengeRecord).build();
    LOG.debug("Calling Google Cloud DNS API to modify zone '{}' with: {}", googleCloudZone, changeRequestInfo);
    final ChangeRequest changeRequest = googleCloudZone.applyChangeRequest(changeRequestInfo);
    waitUntilDoneWith(changeRequest);
    return new DnsSetupImpl(challenge, googleCloudZone, dnsSetupVerificationCallback);
  }

  private static void waitUntilDoneWith(final ChangeRequest changeRequest) {
    while (!changeRequest.isDone()) {
      try {
        Thread.sleep(250L);
      } catch (final InterruptedException e) {
        LOG.warn("Google Cloud DNS change request has been interrupted");
        currentThread().interrupt();
      }
    }
  }

  @Nonnull
  private Zone findGoogleCloudManagedZoneMatching(@Nonnull final Id4Me identity) {
    setupGoogleCloudDnsIfNecessary();
    final String identityFQDN = identity.toFullyQualifiedDnsName();
    final Page<Zone> zoneList;
    synchronized (goodleCloudDnsGuard) {
      zoneList = googleCloudDns.listZones();
    }
    return stream(zoneList.iterateAll().spliterator(), NOT_IN_PARALLEL)
            .filter(zone -> identityFQDN.endsWith(zone.getDnsName()))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Found no Google Cloud DNS Zone responsible for ID4me '" + identity + "'"));
  }

  @Immutable
  private static final class DnsSetupImpl implements DnsSetup {

    private static final Duration HALF_A_SECOND = Duration.ofMillis(500L);

    private final Challenge challenge;
    private final Zone googleCloudZone;
    private final DnsSetupVerificationCallback dnsSetupVerificationCallback;

    private DnsSetupImpl(@Nonnull final Challenge challenge,
                         @Nonnull final Zone googleCloudZone,
                         @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) {
      this.challenge = Validate.notNull(challenge, "Missing ACME challenge");
      this.googleCloudZone = Validate.notNull(googleCloudZone, "Missing Google Cloud DNS zone");
      this.dnsSetupVerificationCallback = Validate.notNull(dnsSetupVerificationCallback, "Missing Identity Authorisation");
    }

    @Nonnull
    @Override
    public Optional<Checked> pollDnsSetupPropagationAt(@Nullable final InetAddress resolverAddress, @Nonnull final Duration timeout) {
      Validate.isTrue(!Validate.notNull(timeout, "Missing timeout").isNegative(), "Negative timeout not allowed");
      final Duration queryInterval;
      int queryCount;
      if (timeout.compareTo(TEN_SECONDS) > 0) {
        Validate.isTrue(timeout.compareTo(MAX_TIMEOUT) <= 0, "Timeout to long: %s > %s", timeout, MAX_TIMEOUT);
        queryInterval = timeout.dividedBy(10L).minus(ASSUMED_DNS_QUERY_LATENCY);
        queryCount = 10;
      } else {
        queryCount = (int) Long.max(timeout.dividedBy(ONE_SECOND), 1L);
        queryInterval = ONE_SECOND.minus(ASSUMED_DNS_QUERY_LATENCY);
      }

      final SimpleResolver resolver;
      try {
        resolver = new SimpleResolver();
      } catch (final UnknownHostException e) {
        throw new RuntimeException("Internal error setting up DNS resolver", e);
      }

      resolver.setAddress(Validate.notNull(resolverAddress, "Missing address of DNS resolver"));
      resolver.setTimeout(HALF_A_SECOND);
      final Message acmeChallengeRecordQuery = prepareQueryForAcmeChallengeOf();
      try {
        do
        {
          Thread.sleep(queryInterval.toMillis());
          try {
            final Message acmeChallengeResponse = resolver.send(acmeChallengeRecordQuery);
            final int responseRCode = acmeChallengeResponse.getHeader().getRcode();
            if (NOERROR == responseRCode) {
              LOG.info("Sucessfully queried for ACME challenge resource record of '{}'",
                      challenge.getIdentifier());
              return afterSuccessfulQueryOfLocalResolverFor(acmeChallengeRecordQuery, queryInterval);
            }
            LOG.debug("Querying for ACME challenge resource record of '{}' failed: {}",
                    challenge.getIdentifier(), Rcode.string(responseRCode));
          } catch (final IOException e) {
            LOG.warn("Querying for ACME challenge resource record of '{}' failed",
                    challenge.getIdentifier(), e);
          }
        } while (--queryCount > 0);
        LOG.warn("Querying for ACME challenge resource record of '{}' timed out",
                challenge.getIdentifier());
      } catch (final InterruptedException e) {
        LOG.warn("Interruption of {}", this, e);
        currentThread().interrupt();
      }
      return empty();
    }

    private Optional<Checked> afterSuccessfulQueryOfLocalResolverFor(@Nonnull final Message acmeChallengeRecordQuery,
                                                                     @Nonnull final Duration queryInterval) {
      final SimpleResolver localResolver;
      try {
        localResolver = new SimpleResolver();
      } catch (final UnknownHostException e) {
        throw new RuntimeException("Internal error setting up local DNS resolver", e);
      }

      localResolver.setTimeout(HALF_A_SECOND);
      int queryCount = 5;
      try {
        do
        {
          Thread.sleep(queryInterval.toMillis());
          try {
            final Message acmeChallengeResponse = localResolver.send(acmeChallengeRecordQuery);
            final int responseRCode = acmeChallengeResponse.getHeader().getRcode();
            if (NOERROR == responseRCode) {
              LOG.debug("Sucessfully queried local resolver for ACME challenge resource record of '{}'",
                      challenge.getIdentifier());
              final Optional<Checked> result = of(new DnsSetupCheckedImpl(challenge, googleCloudZone,
                      dnsSetupVerificationCallback));
              Thread.sleep(2L * queryInterval.toMillis()); // Just to be much more sure that also AIM servers sees it!
              return result;
            }
            LOG.info("Querying local resolver for ACME challenge resource record of '{}' failed: {}",
                    challenge.getIdentifier(), Rcode.string(responseRCode));
          } catch (final IOException e) {
            LOG.warn("Querying local resolver for ACME challenge resource record of '{}' failed",
                    challenge.getIdentifier(), e);
          }
        } while (--queryCount > 0);
      } catch (final InterruptedException e) {
        LOG.warn("Interruption of {}", this, e);
        currentThread().interrupt();
      }
      return empty();
    }

    private Message prepareQueryForAcmeChallengeOf() {
      final Name acmeChallengeName;
      try {
        acmeChallengeName = Name.concatenate(ACME_CHALLENGE,
                Name.fromConstantString(challenge.getIdentifier().toFullyQualifiedDnsName()));
      } catch (final NameTooLongException e) {
        throw new RuntimeException("Internal error preparing ACME challenge query DNS message", e);
      }

      return newQuery(TXTRecord.newRecord(acmeChallengeName, Type.TXT, IN));
    }

    @Override
    public String toString() {
      return "DNS setup of " + challenge;
    }

  }

  @Immutable
  private static class DnsSetupCheckedImpl implements DnsSetup.Checked {

    private final Challenge challenge;
    private final Zone googleCloudZone;
    private final DnsSetupVerificationCallback dnsSetupVerificationCallback;

    private DnsSetupCheckedImpl(@Nonnull final Challenge challenge,
                                @Nonnull final Zone googleCloudZone,
                                @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) {
      this.challenge = Validate.notNull(challenge, "Missing ACME challenge");
      this.googleCloudZone = Validate.notNull(googleCloudZone, "Missing Google Cloud DNS zone");
      this.dnsSetupVerificationCallback = Validate.notNull(dnsSetupVerificationCallback,
              "Missing DNS setup verification callback");
    }

    @Nonnull
    @Override
    public Identity issueDnsSetupVerification() {
      final Identity verifiedIdentity = dnsSetupVerificationCallback.verifyDnsSetup();
      setupOpenIDAndRemoveAcmeChallengeDNSRecord();
      return verifiedIdentity;
    }

    private void setupOpenIDAndRemoveAcmeChallengeDNSRecord() throws DnsSetupException {
      final Id4Me identifier = challenge.getIdentifier();
      final RecordSet openIdRecord = RecordSet
              .newBuilder("_openid." + identifier.toFullyQualifiedDnsName(), RecordSet.Type.TXT)
              .setTtl(1, HOURS)
              .addRecord("\"v=OID1;iss=id.staging.denic.de\"")
              .build();
      final ChangeRequestInfo.Builder changeRequestBuilder = ChangeRequestInfo.newBuilder().add(openIdRecord);
      final Page<RecordSet> acmeChallengeRecordPage = googleCloudZone.listRecordSets(dnsName(ACME_CHALLENGE_PREFIX + identifier.toFullyQualifiedDnsName()));
      stream(acmeChallengeRecordPage.iterateAll().spliterator(), NOT_IN_PARALLEL)
              .filter(recordSet -> RecordSet.Type.TXT.equals(recordSet.getType()))
              .forEach(changeRequestBuilder::delete);
      final ChangeRequestInfo changeRequestInfo = changeRequestBuilder.build();
      LOG.debug("Calling Google Cloud DNS API to modify zone '{}' with: {}", googleCloudZone, changeRequestInfo);
      final ChangeRequest changeRequest = googleCloudZone.applyChangeRequest(changeRequestInfo);
      waitUntilDoneWith(changeRequest);
    }

    @Override
    public String toString() {
      return "Checked for propagation of ACME challenge record DNS setup of " + challenge;
    }

  }

}
