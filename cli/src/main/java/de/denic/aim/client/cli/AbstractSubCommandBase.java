/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.http.AimClient;
import de.denic.aim.client.http.AimClientException;
import de.denic.aim.client.http.impl.AimClientMicronautBasedImpl;
import io.micronaut.context.exceptions.ConfigurationException;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.exceptions.HttpClientException;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.ParentCommand;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.Callable;

import static de.denic.aim.client.cli.Command.activateVerboseLogging;
import static java.util.Optional.ofNullable;

@ThreadSafe
public abstract class AbstractSubCommandBase implements Callable<Integer> {

  public static final String DEFAULT_AGENT_CONFIG_JSON_FILE_NAME = "agent-config.json";

  private static final Logger LOG = LoggerFactory.getLogger(AbstractSubCommandBase.class);
  private static final File DEFAULT_CONFIG_JSON_FILE;
  public static final int CONFIG_ERROR = 1;

  static {
    try {
      DEFAULT_CONFIG_JSON_FILE = new File(".", DEFAULT_AGENT_CONFIG_JSON_FILE_NAME).getCanonicalFile();
    } catch (IOException e) {
      throw new ExceptionInInitializerError(e.getMessage());
    }
  }

  @CommandLine.Option(names = {"-c", "--configFile"},
          description = {"Optional, JSON-formatted file to read/write agent-specific configs from/to.",
                  "Defaults to '${DEFAULT-VALUE}'"},
          paramLabel = "<CONFIG-FILE>")
  private File agentConfigFile = DEFAULT_CONFIG_JSON_FILE;

  @ParentCommand
  private volatile Command parent;

  private final Object agentConfigGuard = new Object();
  @GuardedBy("agentConfigGuard")
  private AgentConfig agentConfig = null;

  @Override
  public final Integer call() {
    if (parent.isVerbose()) {
      activateVerboseLogging();
    }
    try {
      return doCall();
    } catch (final ConfigurationException e) {
      logAsErrorOrDebug(e);
      return CONFIG_ERROR;
    } catch (final HttpClientException e) {
      logAsErrorOrDebug(e);
      return 2;
    } catch (final AimClientException e) {
      logAsErrorOrDebug(e);
      return 3;
    } catch (final RuntimeException e) {
      logAsErrorOrDebug(e);
      return 10;
    } finally {
      parent.close();
    }
  }

  public void logAsErrorOrDebug(final Exception e) {
    if (parent.isVerbose()) {
      LOG.debug(e.getMessage(), e);
    } else {
      LOG.error(e.getMessage());
    }
  }

  protected abstract Integer doCall() throws ConfigurationException, HttpClientException, AimClientException;

  @Nonnull
  protected final File getAgentConfigFile() {
    return agentConfigFile;
  }

  @Nonnull
  protected final Command getParent() {
    return parent;
  }

  @Nonnull
  protected final Optional<AgentConfig> getAgentConfig() {
    synchronized (agentConfigGuard) {
      if (agentConfig == null || !agentConfig.alreadyLoadedFrom(agentConfigFile)) {
        agentConfig = AgentConfig.loadFromFile(agentConfigFile).orElse(null);
      }
      return ofNullable(agentConfig);
    }
  }

  @Nonnull
  protected final AgentConfig getMandatoryAgentConfig() {
    return getAgentConfig().orElseThrow(() -> new ConfigurationException("Missing mandatory agent config file!"));
  }

  @Nonnull
  protected final AgentConfig changeConfigTo(@Nonnull final AgentConfig newConfig) {
    synchronized (agentConfigGuard) {
      this.agentConfig = newConfig;
    }
    return newConfig;
  }

  @Nonnull
  protected final AimClient prepareAimClientFrom(@Nonnull final AgentConfig agentConfig) {
    final URI targetAimServerURI = Validate.notNull(agentConfig, "Missing agent configuration")
            .getTargetAimServerURI()
            .orElseThrow(() -> new ConfigurationException("Agent configuration misses required AIM target server URI"));
    final JWK keyPair = agentConfig.getFirstKeyPair()
            .orElseThrow(() -> new ConfigurationException("Agent configuration misses at least one key-pair"));
    return prepareAimClientFrom(targetAimServerURI, keyPair);
  }

  @Nonnull
  protected final AimClient prepareAimClientFrom(@Nonnull final URI aimServerURI, @Nonnull final JWK keyPair) {
    final URL targetAimServerURL;
    try {
      targetAimServerURL = aimServerURI.toURL();
    } catch (final MalformedURLException e) {
      throw new ConfigurationException("Misconfigured AIM server URI '" + aimServerURI + "': " + e.getMessage());
    }

    return new AimClientMicronautBasedImpl(targetAimServerURL, keyPair, prepareHttpClientFrom(targetAimServerURL));
  }

  @Nonnull
  private HttpClient prepareHttpClientFrom(@Nonnull final URL targetAimServerURL) {
    final RxHttpClient httpClient = RxHttpClient.create(targetAimServerURL);
    getParent().registerAsCloseable(httpClient);
    return httpClient;
  }

}
