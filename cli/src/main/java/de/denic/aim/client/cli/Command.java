/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import ch.qos.logback.classic.Level;
import io.micronaut.context.exceptions.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Option;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import javax.validation.constraints.NotEmpty;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;

import static ch.qos.logback.classic.Level.DEBUG;
import static ch.qos.logback.classic.Level.TRACE;

@ThreadSafe
@CommandLine.Command(name = "java -jar aim.jar",
        header = {"@|green  +--, +--- +   + -+- ,---,         ,---,|@",
                "@|green  |  | |    |\\  |  |  |             ||@",
                "@|green  |  | |--  | \\ |  |  |       ,---, |  -,|@",
                "@|green  |  | |    |  \\|  |  |       +---' |   ||@",
                "@|green  +--' +--- +   + -+- '---'   '---  '---'|@"},
        description = {"Command line tool to issue requests to an DENIC ID AIM (Agent Identity Management) endpoint.",
                "Configuration is laid down in JSON files, the default one is called '" + Command.DEFAULT_AGENT_CONFIG_JSON_FILE_NAME + "'.",
                "Registering a new Agent will create such a file filling it with the appropriate config items " +
                        "(backing up an already existing file by suffixing its content's Agent ID)."},
        mixinStandardHelpOptions = true,
        versionProvider = Command.VersionProvider.class,
        subcommands = {AgentInit.class,
                AgentRead.class,
                IdentityAuthorisationIssue.class,
                IdentityAuthorisationRead.class,
                IdentityAuthorisationVerify.class/*,
                AgentUpdate.class,
                KeyPairCreate.class,
                IdentityRead.class*/})
public class Command implements Callable<Integer>, AutoCloseable {

  public static final String DEFAULT_AGENT_CONFIG_JSON_FILE_NAME = "agent-config.json";
  public static final File DEFAULT_JWK_FILE = new File("aim-keypair.jwk");
  public static final Integer SUCCESS = 0;

  private static final Logger LOG = LoggerFactory.getLogger(Command.class);

  @Option(names = {"-v", "--verbose"},
          description = "Emits more verbose debug messages")
  private volatile boolean verbose = false;

  private final Collection<Closeable> closeables = new CopyOnWriteArrayList<>();

  public static void main(final String[] args) {
    final Integer exitCode = CommandLine.call(new Command(), args);
    System.exit(isExecutedByTestCase() ? SUCCESS : (exitCode == null ? 1 : exitCode));
  }

  @Override
  public Integer call() {
    if (isVerbose()) {
      activateVerboseLogging();
    }
    try {
      return SUCCESS;
    } catch (final ConfigurationException e) {
      LOG.error(e.getMessage());
      return 1;
    }
  }

  private static boolean isExecutedByTestCase() {
    final String testCaseProperty = System.getProperty("test-case");
    final boolean result = "yes".equalsIgnoreCase(testCaseProperty);
    LOG.debug("System property 'test-case' is set to '{}', so {}.",
            testCaseProperty, (result ? "TEST CASE" : "NO test case"));
    return result;
  }

  public boolean isVerbose() {
    return verbose;
  }

  public static void activateVerboseLogging() {
    setLevelOfLogger("io.micronaut.http.client", Level.TRACE);
    setLevelOfLogger("de.denic", Level.DEBUG);
  }

  private static void setLevelOfLogger(@NotEmpty final String loggerName, @Nonnull final Level level) {
    final Logger logger = LoggerFactory.getLogger(loggerName);
    if (logger instanceof ch.qos.logback.classic.Logger) {
      ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) logger;
      logbackLogger.setLevel(level);
    }
  }

  @Override
  public void close() {
    closeables.forEach(closeable -> {
      try {
        closeable.close();
      } catch (final IOException e) {
        LOG.warn("Closing resource {} failed!", closeable, e);
      }
    });
  }

  void registerAsCloseable(@Nullable final Closeable closeable) {
    if (closeable != null) {
      closeables.add(closeable);
    }
  }

  @Immutable
  public final static class VersionProvider implements CommandLine.IVersionProvider {

    private static final String VERSION_JVM_LINE = "JVM: " + System.getProperty("java.version") +
            " (" + System.getProperty("java.vendor") +
            ", " + System.getProperty("java.vm.name") +
            ", " + System.getProperty("java.vm.version") + ")";
    private static final String VERSION_OS_LINE = "OS: " + System.getProperty("os.name") +
            " " + System.getProperty("os.version") +
            " " + System.getProperty("os.arch");

    @Override
    public String[] getVersion() {
      return new String[]{getClass().getPackage().getImplementationTitle() + ", version: " + getClass().getPackage().getImplementationVersion(),
              VERSION_JVM_LINE,
              VERSION_OS_LINE};
    }

  }

}
