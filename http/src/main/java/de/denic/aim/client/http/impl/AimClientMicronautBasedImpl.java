/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import de.denic.aim.client.AgentData;
import de.denic.aim.client.AgentId;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.http.*;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.uri.UriBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.net.URL;
import java.util.Locale;
import java.util.Optional;

import static io.micronaut.http.HttpStatus.*;
import static io.micronaut.http.MediaType.APPLICATION_JSON_TYPE;
import static java.util.Optional.empty;

@Immutable
public final class AimClientMicronautBasedImpl implements AimClient {

  private static final Logger LOG = LoggerFactory.getLogger(AimClientMicronautBasedImpl.class);
  private static final Logger READ_LOG = LoggerFactory.getLogger(AimClientMicronautBasedImpl.class.getName() + "_read");
  private static final String AIM_V2_PATH = "/aim/v2";
  private static final String AIM_V2_AGENTS_PATH = AIM_V2_PATH + "/agents";
  private static final String AIM_V2_AUTHORISATIONS_PATH = AIM_V2_PATH + "/authz";
  private static final String AIM_V2_IDENTITY_AUTHORISATIONS_PATH = AIM_V2_AUTHORISATIONS_PATH + "/id";
  private static final String AIM_V2_IDENTITIES_PATH = AIM_V2_PATH + "/identities";
  private static final String AIM_V2_SUBJECTS_PATH = AIM_V2_PATH + "/subject";
  private static final String EMPTY_PAYLOAD = StringUtils.EMPTY;
  private static final ObjectWriter AGENT_DATA_JSON_WRITER = new ObjectMapper()
          .registerModule(new Jdk8Module()).writerFor(AgentData.class);
  private static final boolean AS_DETACHED_SIGNATURE = true;
  private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;

  private final HttpClient httpClient;
  private final URL baseURL;
  private final JWK publicJWK;
  private final JWSSigner jwsSigner;

  /**
   * @param baseURL    Required
   * @param keyPair    Required; a key ID ("kid") must be available via {@link JWK#getKeyID()}.
   * @param httpClient Required
   */
  public AimClientMicronautBasedImpl(@Nonnull final URL baseURL,
                                     @Nonnull final JWK keyPair,
                                     @Nonnull final HttpClient httpClient) throws IllegalArgumentException {
    this.httpClient = Validate.notNull(httpClient, "Missing HTTP client");
    this.baseURL = Validate.notNull(baseURL, "Missing base URL");
    Validate.notNull(keyPair, "Missing key-pair");
    Validate.notEmpty(keyPair.getKeyID(), "Key-pair misses Key ID");
    Validate.isTrue(KeyUse.SIGNATURE.equals(keyPair.getKeyUse()),
            "Key-pair shows 'use' param as '%s' instead of '%s'", keyPair.getKeyUse(), KeyUse.SIGNATURE);
    Validate.isTrue(keyPair instanceof RSAKey || keyPair instanceof ECKey,
            "Unsupported type of key-pair: %s", keyPair.getClass().getSimpleName());
    this.publicJWK = Validate.notNull(keyPair.toPublicJWK(), "Missing RSA public key");
    try {
      this.jwsSigner = (keyPair instanceof RSAKey ? new RSASSASigner((RSAKey) keyPair) :
              new ECDSASigner((ECKey) keyPair));
    } catch (final JOSEException e) {
      throw new IllegalArgumentException("Provided RSA key inappropriate, cause: " + e.getMessage());
    }
  }

  @Nonnull
  @Override
  public AgentData registerAgentWith(@NotEmpty final String name,
                                     @NotEmpty final String fullName,
                                     @Nonnull @Email final String email,
                                     @Nonnull final URI issuerURI) {
    LOG.debug("Registering Agent with name '{}', full name '{}', email '{}', issuer {}",
            name, fullName, email, issuerURI);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm()).path(AIM_V2_AGENTS_PATH).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("POST", targetURI);
    final ObjectNode jsonObject = JSON_NODE_FACTORY.objectNode()
            .put("name", name)
            .put("full_name", fullName)
            .put("email", email)
            .put("issuer_url", issuerURI.toString());
    jsonObject.putArray("keys").addPOJO(publicJWK.toJSONObject());
    final String jsonPayload = jsonObject.toString();
    final String detachedSerialization = signAndSerializeAsDetached(jwsHeader, jsonPayload);
    final HttpRequest<?> request = HttpRequest.POST(targetURI, jsonPayload)
            .contentType(APPLICATION_JSON_TYPE)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedSerialization);
    final HttpResponse<AgentData.Impl> httpResponse = httpClient.toBlocking().exchange(request, AgentData.Impl.class);
    final HttpStatus responseStatus = httpResponse.getStatus();
    if (responseStatus == CREATED) {
      LOG.info("Registered {}", httpResponse.getBody().map(AgentData::toString).orElse("<UNKNOWN>"));
      return httpResponse.getBody().orElseThrow(() -> new RuntimeException("Internal error"));
    }

    throw new AimClientException("Registration of Agent failed. HTTP response code '" + responseStatus
            + "'. Body: " + httpResponse.getBody(String.class).orElse("<EMPTY>"));
  }

  @Nonnull
  @Override
  public Optional<? extends AgentData> loadDataOfAgentWith(@Nonnull final AgentId agentId) {
    READ_LOG.debug("Loading data of Agent '{}'", agentId);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_AGENTS_PATH)
            .path(Validate.notNull(agentId, "Missing Agent ID").toString()).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("GET", targetURI);
    final String detachedJWSSerialisation = signAndSerializeEmptyPayload(jwsHeader);
    final HttpRequest<?> request = HttpRequest.GET(targetURI)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    Optional<? extends AgentData> result = empty();
    try {
      final HttpResponse<? extends AgentData> httpResponse = httpClient.toBlocking().exchange(request, AgentData.Impl.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus == OK) {
        result = httpResponse.getBody();
      } else {
        throw new AimClientException("Lookup of Agent '" + agentId + "' failed. HTTP response status '" + responseStatus
                + "'. Body: " + httpResponse.getBody(String.class).orElse("<EMPTY>"));
      }
    } catch (final HttpClientResponseException e) {
      switch (e.getStatus()) {
        case NOT_FOUND: // fallsthrough
        case BAD_REQUEST:
          break;

        default:
          throw new AimClientException("Lookup of Agent '" + agentId + "' failed. Response status '" + e.getStatus() +
                  "', message: '" + e.getMessage() + "'");
      }
    }
    READ_LOG.info("Found data of Agent '{}' as: {}", agentId, result.map(AgentData::toString).orElse("<UNKNOWN>"));
    return result;
  }

  @Nonnull
  @Override
  public AgentData updateAgentTo(@Nonnull final AgentData modifiedAgent) throws AimClientException {
    LOG.debug("Modifying {}", modifiedAgent);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_AGENTS_PATH)
            .path(modifiedAgent.getId().toString()).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("PUT", targetURI);
    final String jwsPayload;
    try {
      jwsPayload = AGENT_DATA_JSON_WRITER.writeValueAsString(modifiedAgent);
    } catch (final JsonProcessingException e) {
      throw new RuntimeException("Internal error while serialising Agent data to JSON", e);
    }

    final String detachedJWSSerialisation = signAndSerializeAsDetached(jwsHeader, jwsPayload);
    final HttpRequest<?> request = HttpRequest.PUT(targetURI, jwsPayload)
            .contentType(APPLICATION_JSON_TYPE)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    final HttpResponse<? extends AgentData> httpResponse = httpClient.toBlocking().exchange(request, AgentData.Impl.class);
    final HttpStatus responseStatus = httpResponse.getStatus();
    if (responseStatus == OK) {
      final AgentData result = httpResponse.getBody().orElseThrow(
              () -> new AimClientException("Deserializing of HTTP response body does not provide modified Agent"));
      LOG.info("Modified: {}", result);
      return result;
    }

    throw new AimClientException("Modification of Agent failed. HTTP response status '" + responseStatus
            + "'. Body: " + httpResponse.getBody(String.class).orElse("<EMPTY>"));
  }

  @Nonnull
  @Override
  public AuthorisationChallenge issueAuthorisationWith(@Nonnull final Id4Me identifier,
                                                       @Nonnull final Locale locale) throws AimClientException {
    LOG.debug("Agent is issuing Authorisation for Identity: '{}', locale '{}'", identifier, locale);
    final JsonNode jsonPayload = JSON_NODE_FACTORY.objectNode()
            .put("identifier", Validate.notNull(identifier, "Missing identifier").getValue())
            .put("locale", Validate.notNull(locale, "Missing locale").getLanguage());
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm()).path(AIM_V2_AUTHORISATIONS_PATH).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("POST", targetURI);
    final String jwsPayload = jsonPayload.toString();
    final String detachedJWSSerialisation = signAndSerializeAsDetached(jwsHeader, jwsPayload);
    final HttpRequest<?> request = HttpRequest.POST(targetURI, jwsPayload)
            .contentType(APPLICATION_JSON_TYPE)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    try {
      final HttpResponse<AuthorisationChallenge.Impl> httpResponse = httpClient.toBlocking().exchange(request, AuthorisationChallenge.Impl.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus == CREATED) {
        LOG.info("Created {}", httpResponse.getBody().map(AuthorisationChallenge::toString).orElse("<UNKNOWN>"));
        return httpResponse.getBody().orElseThrow(() -> new RuntimeException("Internal error"));
      }

      throw new AimClientException("Issuance of an Identity Authorisation failed. HTTP response status '" + responseStatus
              + "'. Body: " + httpResponse.getBody(String.class).orElse("<EMPTY>"));
    } catch (final HttpClientResponseException e) {
      throw new AimClientException("Issuance of an Identity Authorisation failed. HTTP response status '" + e.getResponse().getStatus()
              + "'. Message: '" + e.getMessage() + "'");
    }
  }

  @Nonnull
  @Override
  public Optional<AuthorisationChallenge> loadIdentityAuthorisationWith(@Nonnull final AuthzID identAuthzId) throws AimClientException {
    READ_LOG.debug("Loading Identity Authorisation '{}'", identAuthzId);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_IDENTITY_AUTHORISATIONS_PATH)
            .path(Validate.notNull(identAuthzId, "Missing Identitiy Authorisation ID").toString()).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("GET", targetURI);
    final String detachedJWSSerialisation = signAndSerializeEmptyPayload(jwsHeader);
    final HttpRequest<?> request = HttpRequest.GET(targetURI)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    Optional<AuthorisationChallenge> result = empty();
    try {
      final HttpResponse<AuthorisationChallenge> httpResponse = httpClient.toBlocking().exchange(request, AuthorisationChallenge.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus == OK) {
        result = httpResponse.getBody();
      } else {
        throw new AimClientException("Lookup of Identity Authorisation '" + identAuthzId +
                "' failed. HTTP response status '" + responseStatus + "'. Body: " +
                httpResponse.getBody(String.class).orElse("<EMPTY>"));
      }
    } catch (final HttpClientResponseException e) {
      if (e.getStatus() != NOT_FOUND) {
        throw new AimClientException("Lookup of Identity Authorisation '" + identAuthzId +
                "' failed. Response status '" + e.getStatus() + "', message: '" + e.getMessage() + "'");
      }
    }
    READ_LOG.info("Found Identity Authorisation of ID '{}' as: {}", identAuthzId,
            result.map(AuthorisationChallenge::toString).orElse("<UNKNOWN>"));
    return result;
  }

  @Nonnull
  @Override
  public IdentityAuthorisation verifyDnsSetupOfIdentityAuthorisation(@Nonnull final AuthzID identAuthzId) throws AimClientException {
    LOG.debug("Agent is issuing DNS setup verification of Identity Authorisation ID '{}'", identAuthzId);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_IDENTITY_AUTHORISATIONS_PATH)
            .path(Validate.notNull(identAuthzId, "Missing Identity Authorisation ID").toString())
            .build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("POST", targetURI);
    final String detachedJWSSerialisation = signAndSerializeEmptyPayload(jwsHeader);
    final HttpRequest<?> request = HttpRequest.POST(targetURI, EMPTY_PAYLOAD)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    try {
      final HttpResponse<IdentityAuthorisation> httpResponse = httpClient.toBlocking().exchange(request,
              IdentityAuthorisation.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (OK == responseStatus) {
        final Optional<IdentityAuthorisation> result = httpResponse.getBody();
        LOG.info("Verified {}", result.map(Object::toString).orElse("<UNKNOWN>"));
        return result.orElseThrow(() -> new RuntimeException("Internal error"));
      }

      throw new AimClientException("Verification issuance of an Identity Authorisation failed. HTTP response status '" +
              responseStatus + "'. Body: " + httpResponse.getBody(String.class).orElse("<EMPTY>"));
    } catch (final HttpClientResponseException e) {
      final String errorMessage;
      switch (e.getStatus()) {
        case NOT_FOUND:
          errorMessage = "Verification of an UNKNOWN Identity Authorisation '" + identAuthzId + "' failed.";
          break;
        case GONE: // FALLTHROUGH
        case PRECONDITION_FAILED:
          errorMessage = "Verification of an Identity Authorisation '" + identAuthzId + "' failed due to: " + e.getMessage();
          break;
        default:
          errorMessage = "Verification issuance of an Identity Authorisation failed. HTTP response status '" + e.getResponse().getStatus()
                  + "'. Message: '" + e.getMessage() + "'";
      }
      throw new AimClientException(errorMessage);
    }
  }

  @Nonnull
  @Override
  public Optional<Identity> loadIdentityBySubject(@NotEmpty final String subject) throws AimClientException {
    return doLoadIdentityBy("Subject", subject);
  }

  @Nonnull
  @Override
  public Optional<Identity> loadIdentityById(@Nonnull final Id4Me identifier) throws AimClientException {
    return doLoadIdentityBy("Identifier", identifier);
  }

  private Optional<Identity> doLoadIdentityBy(@NotEmpty final String fieldName,
                                              @NotEmpty final Object fieldValue) {
    READ_LOG.debug("Agent is loading Identity by {} '{}'", fieldName, fieldValue);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_IDENTITIES_PATH)
            .path(Validate.notNull(fieldValue, "Missing Identity field value").toString()).build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("GET", targetURI);
    final String detachedJWSSerialisation = signAndSerializeEmptyPayload(jwsHeader);
    final HttpRequest<?> request = HttpRequest.GET(targetURI)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    Optional<Identity> result = empty();
    try {
      final HttpResponse<Identity> httpResponse = httpClient.toBlocking().exchange(request, Identity.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus == OK) {
        result = httpResponse.getBody();
      } else {
        throw new AimClientException("Lookup of Identity with " + fieldName + " '" + fieldValue +
                "' failed. HTTP response status '" + responseStatus + "'. Body: " +
                httpResponse.getBody(String.class).orElse("<EMPTY>"));
      }
    } catch (final HttpClientResponseException e) {
      if (e.getStatus() != NOT_FOUND) {
        throw new AimClientException("Lookup of Identity with " + fieldName + " '" + fieldValue +
                "' failed. Response status '" + e.getStatus() + "', message: '" + e.getMessage() + "'");
      }
    }
    READ_LOG.info("Found Identity with {} '{}' as: {}", fieldName, fieldValue,
            result.map(Identity::toString).orElse("<UNKNOWN>"));
    return result;
  }

  @Nonnull
  @Override
  public PasswordSetup triggerPasswordSetupOf(@NotEmpty final String subject) throws AimClientException {
    LOG.debug("Agent triggers password reset workflow of Identity Subject '{}'", subject);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_IDENTITIES_PATH)
            .path("credentialsReset")
            .build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("POST", targetURI);
    final JsonNode jsonObject = JSON_NODE_FACTORY.objectNode()
            .put("sub", Validate.notEmpty(subject, "Missing subject"));
    final String jwsPayload = jsonObject.toString();
    final String detachedJWSSerialisation = signAndSerializeAsDetached(jwsHeader, jwsPayload);
    final HttpRequest<?> request = HttpRequest.POST(targetURI, jwsPayload)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    try {
      final HttpResponse<PasswordSetup> httpResponse = httpClient.toBlocking().exchange(request, PasswordSetup.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus != OK) {
        throw new AimClientException("Triggered password reset workflow of Identity with Subject '" + subject +
                "' failed. HTTP response status '" + responseStatus + "'.");
      }

      LOG.info("Triggered password reset workflow of Identity Subject '{}'", subject);
      return httpResponse.body();
    } catch (final HttpClientResponseException e) {
      throw new AimClientException("Triggered password reset workflow of Identity with Subject '" + subject +
              "' failed. Response status '" + e.getStatus() + "', message: '" + e.getMessage() + "'");
    }
  }

  @Override
  public boolean deletedIdentityFromSubject(@Nonnull final String subject,
                                            @Nonnull final Id4Me identifier) throws AimClientException {
    LOG.debug("Agent is deleting Identity '{}' from Subject '{}'", identifier, subject);
    final URI targetURI = UriBuilder.of(baseURL.toExternalForm())
            .path(AIM_V2_SUBJECTS_PATH)
            .path(Validate.notEmpty(subject, "Missing Subject"))
            .path("id")
            .path(Validate.notNull(identifier, "Missing ID4me identifier").getValue())
            .build();
    final JWSHeader jwsHeader = createJwsHeaderWithKID("DELETE", targetURI);
    final String detachedJWSSerialisation = signAndSerializeEmptyPayload(jwsHeader);
    final HttpRequest<?> request = HttpRequest.DELETE(targetURI, EMPTY_PAYLOAD)
            .accept(APPLICATION_JSON_TYPE)
            .header("jws-detached-signature", detachedJWSSerialisation);
    try {
      final HttpResponse<String> httpResponse = httpClient.toBlocking().exchange(request, String.class);
      final HttpStatus responseStatus = httpResponse.getStatus();
      if (responseStatus == OK || responseStatus == NO_CONTENT) {
        LOG.info("Successfully deleted identifier '{}' from Subject '{}'", identifier, subject);
        return true;
      }
      throw new AimClientException("Deleting Identity with ID/Subject '" + identifier + "'/'" + subject +
              "' failed. Response status '" + responseStatus + "', body: '" + httpResponse.body() + "'");
    } catch (final HttpClientResponseException e) {
      final HttpStatus httpStatus = e.getStatus();
      if (httpStatus == NOT_FOUND) {
        LOG.info("NO identifier '{}' from Subject '{}' available for deletion", identifier, subject);
        return false;
      }
      throw new AimClientException("Deleting Identity with ID/Subject '" + identifier + "'/'" + subject +
              "' failed. Response status '" + e.getStatus() + "', message: '" + e.getMessage() + "'");
    }
  }

  private String signAndSerializeAsDetached(final JWSHeader jwsHeader, final String jwsPayload) {
    return signAndSerializeAsDetached(jwsHeader, new Payload(jwsPayload));
  }

  private String signAndSerializeEmptyPayload(final JWSHeader jwsHeader) {
    return signAndSerializeAsDetached(jwsHeader, EMPTY_PAYLOAD);
  }

  private String signAndSerializeAsDetached(final JWSHeader jwsHeader, final Payload payload) {
    final JWSObject jwsInstance = new JWSObject(jwsHeader, payload);
    try {
      jwsInstance.sign(jwsSigner);
    } catch (final JOSEException e) {
      throw new RuntimeException("Internal error", e);
    }

    return jwsInstance.serialize(AS_DETACHED_SIGNATURE);
  }

  private JWSHeader createJwsHeaderWithKID(@NotEmpty final String httpMethod,
                                           @Nonnull final URI targetURI) {
    return new JWSHeader.Builder(jwsSigner.supportedJWSAlgorithms().stream().findFirst().orElseThrow())
            .customParam("method", httpMethod)
            .customParam("url", targetURI.toASCIIString())
            .keyID(publicJWK.getKeyID())
            .build();
  }

  @Override
  public String toString() {
    return "AIMClient ('" + baseURL +
            "', publicJWK=" + publicJWK +
            '}';
  }

}
