/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.denic.aim.client.Id4Me;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.aim.client.http.AuthorisationChallenge.Util.*;
import static de.denic.aim.client.http.Challenge.Util.*;
import static java.time.temporal.ChronoField.NANO_OF_SECOND;

@JsonDeserialize(as = AuthorisationChallenge.Impl.class)
@JsonSerialize(as = AuthorisationChallenge.Impl.class)
public interface AuthorisationChallenge extends Challenge {

  DateTimeFormatter OPTIONAL_FRACTION_OF_SECONDS_FORMATTER = new DateTimeFormatterBuilder()
          .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
          .appendFraction(NANO_OF_SECOND, 0, 9, true)
          .appendPattern("X")
          .toFormatter();

  @Nonnull
  AuthzID getId();

  @Nonnull
  ZonedDateTime getExpirationTS();

  @Nonnull
  Locale getLanguage();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @JsonPropertyOrder({AUTHZ_ID_KEY, IDENTIFIER_KEY, EXPIRATION_TS_KEY, VALIDATION_TS_KEY, DNS_CHALLENGE_TOKEN_KEY, LOCALE_KEY})
  @Immutable
  final class Impl extends Challenge.Impl implements AuthorisationChallenge {

    private final AuthzID id;
    private final ZonedDateTime expirationTS;
    private final Locale language;

    public Impl(@Nonnull final AuthzID id,
                @Nonnull final Id4Me identifier,
                @Nonnull final ZonedDateTime expirationTS,
                @Nullable final ZonedDateTime validationTS,
                @NotEmpty final String dnsChallengeToken,
                @Nonnull final Locale language) {
      super(identifier, dnsChallengeToken, validationTS);
      this.id = Validate.notNull(id, "Missing ID");
      this.expirationTS = Validate.notNull(expirationTS, "Missing expiration date");
      this.language = Validate.notNull(language, "Missing locale/language");
    }

    @JsonCreator
    public Impl(@JsonSetter(AUTHZ_ID_KEY) @Nonnull final AuthzID id,
                @JsonSetter(IDENTIFIER_KEY) @Nonnull final Id4Me identifier,
                @JsonSetter(EXPIRATION_TS_KEY) @NotEmpty final String expirationTSValue,
                @JsonSetter(VALIDATION_TS_KEY) @Nullable final String validationTSValue,
                @JsonSetter(DNS_CHALLENGE_TOKEN_KEY) @NotEmpty final String dnsChallengeToken,
                @JsonSetter(LOCALE_KEY) @Nonnull final Locale language) {
      this(id, identifier,
              ZonedDateTime.from(OPTIONAL_FRACTION_OF_SECONDS_FORMATTER.parse(expirationTSValue)),
              StringUtils.isEmpty(validationTSValue) ? null :
                      ZonedDateTime.from(OPTIONAL_FRACTION_OF_SECONDS_FORMATTER.parse(validationTSValue)),
              dnsChallengeToken,
              language);
    }

    @JsonGetter(AUTHZ_ID_KEY)
    @Nonnull
    @Override
    public AuthzID getId() {
      return id;
    }

    @JsonIgnore
    @Nonnull
    @Override
    public ZonedDateTime getExpirationTS() {
      return expirationTS;
    }

    @JsonGetter(EXPIRATION_TS_KEY)
    @NotEmpty
    public String getExpirationTSAsJSON() {
      return OPTIONAL_FRACTION_OF_SECONDS_FORMATTER.format(expirationTS);
    }

    @JsonGetter(LOCALE_KEY)
    @Nonnull
    @Override
    public Locale getLanguage() {
      return language;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final AuthorisationChallenge.Impl impl = (AuthorisationChallenge.Impl) o;

      if (!id.equals(impl.id))
        return false;
      if (!expirationTS.equals(impl.expirationTS))
        return false;
      return language.equals(impl.language);
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + id.hashCode();
      result = 31 * result + expirationTS.hashCode();
      result = 31 * result + language.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "Authorisation " + id +
              " (" + super.toString() +
              ", locale=" + language +
              ", expires=" + expirationTS +
              ')';
    }

  }

  final class Util {

    static final String AUTHZ_ID_KEY = "identifier_authz_id";
    static final String EXPIRATION_TS_KEY = "expiration_ts";
    static final String LOCALE_KEY = "locale";

  }

}
