/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.denic.aim.client.Id4Me;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.aim.client.http.IdentityAuthorisation.Util.*;
import static java.time.temporal.ChronoField.NANO_OF_SECOND;

@JsonDeserialize(as = IdentityAuthorisation.Impl.class)
@JsonSerialize(as = IdentityAuthorisation.Impl.class)
public interface IdentityAuthorisation {

  DateTimeFormatter OPTIONAL_FRACTION_OF_SECONDS_FORMATTER = new DateTimeFormatterBuilder()
          .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
          .appendFraction(NANO_OF_SECOND, 0, 9, true)
          .appendPattern("X")
          .toFormatter();

  @Nonnull
  AuthzID getId();

  @Nonnull
  Id4Me getIdentifier();

  @Nonnull
  String getSubject();

  @Nonnull
  Locale getLanguage();

  @Nonnull
  URL getCredentialsInitURL();

  @Nonnull
  ZonedDateTime getCredentialsInitExpirationTS();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @JsonPropertyOrder({AUTHZ_ID_KEY, IDENTIFIER_KEY, SUBJECT_KEY, LOCALE_KEY, CREDENTIALS_INIT_URI_KEY, CREDENTIALS_INIT_EXPIRATION_TS_KEY})
  @Immutable
  final class Impl implements IdentityAuthorisation {

    private final Id4Me identifier;
    private final AuthzID id;
    private final Locale language;
    private final String subject;
    private final URL credentialsInitURL;
    private final ZonedDateTime credentialsInitExpirationTS;

    public Impl(@Nonnull final AuthzID id,
                @Nonnull final Id4Me identifier,
                @Nonnull final Locale language,
                @NotEmpty final String subject,
                @Nonnull final URL credentialsInitURL,
                @Nonnull final ZonedDateTime credentialsInitExpirationTS) {
      this.id = Validate.notNull(id, "Missing ID");
      this.identifier = Validate.notNull(identifier, "Missing identifier");
      this.language = Validate.notNull(language, "Missing locale/language");
      this.subject = Validate.notEmpty(subject, "Missing subject");
      this.credentialsInitURL = Validate.notNull(credentialsInitURL, "Missing credentials initialization URL");
      this.credentialsInitExpirationTS = Validate.notNull(credentialsInitExpirationTS, "Missing credentials initialization expiration timestamp");
    }

    @JsonCreator
    public Impl(@JsonSetter(AUTHZ_ID_KEY) @Nonnull final AuthzID id,
                @JsonSetter(IDENTIFIER_KEY) @Nonnull final Id4Me identifier,
                @JsonSetter(LOCALE_KEY) @Nonnull final Locale language,
                @JsonSetter(SUBJECT_KEY) @NotEmpty final String subject,
                @JsonSetter(CREDENTIALS_INIT_URI_KEY) @Nonnull final URL credentialsInitURL,
                @JsonSetter(CREDENTIALS_INIT_EXPIRATION_TS_KEY) @NotEmpty final String credentialsInitExpirationTSValue) {
      this(id, identifier,
              language,
              subject,
              credentialsInitURL,
              ZonedDateTime.from(OPTIONAL_FRACTION_OF_SECONDS_FORMATTER.parse(credentialsInitExpirationTSValue)));
    }

    @JsonGetter(AUTHZ_ID_KEY)
    @Nonnull
    @Override
    public AuthzID getId() {
      return id;
    }

    @JsonGetter(IDENTIFIER_KEY)
    @Nonnull
    @Override
    public Id4Me getIdentifier() {
      return identifier;
    }

    @JsonGetter(LOCALE_KEY)
    @Nonnull
    @Override
    public Locale getLanguage() {
      return language;
    }

    @JsonGetter(SUBJECT_KEY)
    @Nonnull
    @Override
    public String getSubject() {
      return subject;
    }

    @JsonGetter(CREDENTIALS_INIT_URI_KEY)
    @Nonnull
    @Override
    public URL getCredentialsInitURL() {
      return credentialsInitURL;
    }

    @JsonGetter(CREDENTIALS_INIT_EXPIRATION_TS_KEY)
    @Nonnull
    @Override
    public ZonedDateTime getCredentialsInitExpirationTS() {
      return credentialsInitExpirationTS;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final Impl impl = (Impl) o;

      if (!id.equals(impl.id))
        return false;
      if (!identifier.equals(impl.identifier))
        return false;
      if (!language.equals(impl.language))
        return false;
      if (!subject.equals(impl.subject))
        return false;
      if (!credentialsInitURL.equals(impl.credentialsInitURL))
        return false;
      return credentialsInitExpirationTS.equals(impl.credentialsInitExpirationTS);
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + id.hashCode();
      result = 31 * result + identifier.hashCode();
      result = 31 * result + language.hashCode();
      result = 31 * result + subject.hashCode();
      result = 31 * result + credentialsInitURL.hashCode();
      result = 31 * result + credentialsInitExpirationTS.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "Solved authorisation " + id +
              " ('" + identifier + "'" +
              ", sub=" + subject +
              ", locale=" + language +
              ", initURL='" + credentialsInitURL + "' (expires " + credentialsInitExpirationTS + ")" +
              ')';
    }

  }

  final class Util {

    static final String AUTHZ_ID_KEY = "identifier_authz_id";
    static final String IDENTIFIER_KEY = "identifier";
    static final String LOCALE_KEY = "locale";
    static final String SUBJECT_KEY = "sub";
    static final String CREDENTIALS_INIT_URI_KEY = "init_url";
    static final String CREDENTIALS_INIT_EXPIRATION_TS_KEY = "init_url_expiration_ts";

  }

}
