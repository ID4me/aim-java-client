# Agent Identity Management (AIM) Java Client

[![pipeline status](https://gitlab.com/ID4me/aim-java-client/badges/master/pipeline.svg)](https://gitlab.com/ID4me/aim-java-client/commits/master)
[![coverage report](https://gitlab.com/ID4me/aim-java-client/badges/master/coverage.svg)](https://gitlab.com/ID4me/aim-java-client/commits/master)

This is a Java implementation of a client library and command-line interface to communicate with an AIM server.

AIM describes a HTTP-based interface to issue creation and management operations of so-called Agents as 
well as ID4me identities created by them.

* [Documentation of DENIC's AIM server endpoint](https://id.denic.de/aim/docs/api/#tag/agents)
* [GitLab Page with docs, reports, build artifacts, etc.](https://id4me.gitlab.io/aim-java-client/)

## Technical Client

At the lowest/technical level there is a Java Component Interface `de.denic.aim.client.http.AimClient` together 
with an implementation class `de.denic.aim.client.http.impl.AimClientMicronautBasedImpl` based on the [HTTP client](https://docs.micronaut.io/latest/guide/index.html#httpClient) of 
the [Micronaut framework](https://micronaut.io).

This achieves handling of the technical issues to communicate with an AIM server.

## Business Client

A more business-like interface is provided with `de.denic.aim.client.business.Agent` interface and it's 
implementation `de.denic.aim.client.business.impl.AgentImpl`, usually created by applying
`de.denic.aim.client.business.Agent.Builder`'s Factory Methods.

## Command-line Interface Client

Last but not least a [Gradle Shadow JAR](https://github.com/johnrengelman/shadow) file is built to issue requests 
from the command-line. Get an overview by issuing

```bash
$ java -jar aim.jar --help
```

General configurations:

* Authorisation with a public/private key-pair file, formatted as
  [JSON Web Key (JWK)](https://tools.ietf.org/html/rfc7517): `--jwk [MY_KEY_PAIR_FILE.jwk]`.
  Per default a local file `./aim-keypair.jwk` is applied. Programm may create a random fresh new
  key pair for you:
  ```bash
  $ java -jar aim.jar keypair-create 2> fresh-new-keypair.jwk
  ```
  **Hint**: It will output the result on the shell's SYSERR output stream (therefor the example
  execution shown above redirects this stream to the file `fresh-new-keypair.jwk`)
  * to **not** overwrite any existing key file, and
  * to not get disturbed by any other debugging/informative output on SYSOUT. 

* Defining the target AIM server's URL by giving a CLI parameter: `--url [AIM_SERVER_URL]`,
  or by defining a local file `./aim-endpoint.txt` containing the URL. If neither is given,
  the *DENIC Staging Environment's* endpoint `https://id.staging.denic.de` is applied as default. 

### Multitenancy

Putting both, a **distinct** JWK file `aim-keypair.jwk` and (optionally) a file `aim-endpoint.txt` and later on
(after registering your Agent, see below) an **accompanying** Agent ID file `agent-id.txt`
in separate (sub-)folders you can achieve handling of many Agents (**multitenancy**) by executing commands in
their own (sub-)folder. Here is an example of a local filesystem structure to handle three different Agents
(*test-agent*, *agent-foo*, *agent-bar*):

```text
/agent-multitenancy
|-- aim.jar
|-- /test-agent
|   |-- aim-keypair.jwk
|   '-- agent-id.txt
+-- /agent-foo
|   |-- aim-endpoint.txt
|   |-- aim-keypair.jwk
|   '-- agent-id.txt
'-- /agent-bar
    |-- aim-endpoint.txt
    |-- aim-keypair.jwk
    '-- agent-id.txt
```

Hint: The *test-agent* doesn't need to declare it's AIM endpoint URL in an appropriate config file, cause
he targets the default *Staging Environment* endpoint [id.staging.denic.de](https://id.staging.denic.de).

Within the subfolder you may issue commands to address each tenant, e.g. with the *test-agent* like this:

```bash
/agent-multitenancy/test-agent$ java -jar ../aim.jar agent-read
```

### Registering an Agent

Prerequisite for registering a new Agent is a public/private Key Pair in [JWK format](https
://tools.ietf.org/html/rfc7517) (in the following example expected in file `aim-keypair.jwk`, the default file name here)!
This Key Pair authorises any further operations on the created Agent instance. 

```bash
$ java -jar aim.jar agent-create \
 --jwk aim-keypair.jwk \
 --name "exampleName" \
 --fullName "Full example" \
 --email "mail@agent.example" \
 --issuerURL "https://agent.example/issuer"
```

To output is something like the following stating that the ID of the newly created Agent is written to file
`agent-id.txt`:

```text
INFO : Read Key Pair from file [...]/aim-keypair.jwk
INFO : Creating Agent: Name 'exampleName', full name 'Full example', email 'mail@agent.example', issuer https://agent.example/issuer
INFO : Created Agent [AGENT-ID] ('exampleName', status=active, full='Full example', email='mail@agent.example', issuer=https://agent.example/issuer)
INFO : Written ID of newly created Agent instance to file [...]/agent-id.txt (UTF-8 encoded)
```

### Show data of an already registered Agent

If you have a file `agent-id.txt` available, simply issue

```bash
$ java -jar aim.jar agent-read
```

to get something like this:

```text
INFO : Read Key Pair from file [...]/aim-keypair.jwk
INFO : Read Agent ID from file [...]/agent-id.txt
INFO : Loading Agent of ID '[AGENT_ID]'
INFO : Found Agent of ID '[AGENT_ID]' as: Agent [AGENT_ID] ('exampleName', status=active, full='Full example', email='mail@agent.example', issuer='https://agent.example/issuer')
```

Otherwise provide the ID of the Agent in question:

```bash
$ java -jar aim.jar agent-read \
 --agent [OTHER_AGENT_ID]
```

### Authorisation of an [ID4me](https://id4me.org) Identity

Authorisation of ID4me Identity requires:
* You have an active Agent account, which means you have
  * an Agent ID (usually put in a file `agent-id.txt`)
  * a Private/Public key pair in JWK format (usually laid down in a file `aim-keypair.jwk`)

(The AIM Server's URL, the file holding your JWK key pair to authorise your request, and the ID of your Agent
 registration could get defined by appropriate programm parameters.)
 
The authorisation of an ID4me Identity consists of three steps:
1. Launch an Authorisation request on behalf of an AIM Agent regarding the Identity in question.
2. Add a provided [ACME DNS Challenge](https://tools.ietf.org/html/rfc8555#section-8.4) to the DNS Domain the
 Identity belongs to.
3. Tell the AIM server to verify that you are in charge of this domain by checking the DNS for the provide challenge.

#### Issue an ID4me Identity Authorisation request

```bash
$ java -jar aim.jar authz-issue --identity [MY_ID4ME_IDENTITY]
```

In response to this request the program will
* write a file `./identity-authorisation.txt` which holds
the technical ID representing your request on the AIM server's side, and
* write a file `./_acme-challenge.[MY_ID4ME_IDENTITY].txt` containing a line to add as
[DNS TXT Resource Record](https://de.wikipedia.org/wiki/TXT_Resource_Record)
to solve the mentioned
[ACME DNS Challenge](https://tools.ietf.org/html/rfc8555#section-8.4). This information is also
written to the console output like this:
`INFO : DNS ACME Challenge resource record to add to your zone file: '[DNS_TXT_RESOURCE_RECORD]'`

An ACME DNS Challenge DNS TXT Resource Record looks something like this:

```
_acme-challenge.cli.test.identity. 300 IN TXT "SeqJTg_HkTH3gF_rm6qw_-yh2yGTC6esVHiip6xcji8"
```

After adding this line to your DNS Zone you are ready to

#### Issue an ID4me Authorisation Verification request

Now usually the ID of the formerly issued Authorisation request is read from file `./identity-authorisation.txt`,
so you simply execute:

```bash
$ java -jar aim.jar authz-verify
```

On success you will find a new file written to your local folder named
`./subject_[MY_ID4ME_IDENTITY].txt` containing
the pseudonym [OpenID Subject Identifier](https://openid.net/specs/openid-connect-core-1_0.html#Terminology)
assigned to your verified ID4me Identity by the AIM server.


## Generating Agent's [JSON Web Key (JWK)](https://tools.ietf.org/html/rfc7517)

To create a JWK key-pair to authorize your Agent's operations you may use a downloadable [CLI software from Connect2Id](https://connect2id.com/products/nimbus-jose-jwt/generator) (or the online version of it).


## Publication

### On [JitPack](https://jitpack.io)

Unfortunately JitPack currently does **not** support GitLab to [build artifacts "ahead of time"](https://jitpack.io/docs/BUILDING/#building-ahead-of-time).
So a build is launched **only** by requests of downstream projects, not by e.g. CI builds on GitLab's infrastructure!

To explicitly trigger a fresh build, you have to tell Gradle to ignore its caching of dependency libs by adding `--refresh-dependencies` flag:

```console
./gradlew --refresh-dependencies [...]
```

#### Lookup of builds available
To get an overview of all available builds on JitPack you can query them by issueing:

```console
$ curl -s https://jitpack.io/api/builds/com.gitlab.ID4me/aim-java-client
{
  "com.gitlab.ID4me" : {
    "aim-java-client" : {
      "master-891c36c394-1" : "Error",
      "master-a23f29e1fc-1" : "ok",
      [...]
    }
  }
}
```

#### Details of an individual build
To get info from the `latest` builds, simply append `latest` to the URI:
```console
$ curl -s https://jitpack.io/api/builds/com.gitlab.ID4me/aim-java-client/latest
{
  [...]
}
```

Or simply append the ID of the build in question like so:
```console
$ curl -s https://jitpack.io/api/builds/com.gitlab.ID4me/aim-java-client/master-a23f29e1fc-1
{
  "version" : "master-a23f29e1fc-1",
  "status" : "ok",
  [...]
}
```

#### Insight into build process' output
Output of some specific build process can be accessed like so: 

```console
$ curl -s https://jitpack.io/com/gitlab/ID4me/aim-java-client/master-45e9c42bb5-1/build.log
Build starting...
Start: Tue Jan 19 14:02:27 UTC 2021 e6a1ac3ccd86
Git:
45e9c42
commit 45e9c42bb53e4fe3a38d406376ad2f7677b47f49
[...]
```

#### Local test of JitPack build process
According to [documentation](https://jitpack.io/docs/BUILDING/) the following command
is executed by JitPack's build infrastructure (cause we apply Gradle's [`maven-publish` Plugin](https://docs.gradle.org/current/userguide/publishing_maven.html)):

```console
./gradlew build publishToMavenLocal  # Or shorter: ./gradlew build pTML
```

By executing this locally you are able to test JitPack's build process!
This way the artifacts are published in your local Maven repository, usually `[HOME]/.m2/repository`.
By upfront deletion of this folder you get a clean target making it easier to check the publishing results in detail.
