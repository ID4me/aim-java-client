/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import de.denic.aim.client.AgentId;
import de.denic.aim.client.http.AimClient;
import org.apache.commons.lang3.StringUtils;
import picocli.CommandLine;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public abstract class AgentIdRequiringSubCommand extends AimClientRequiringSubCommand {

  @CommandLine.Option(names = {"-a", "--agent"},
          description = "ID of the Agent.",
          paramLabel = "<ID>")
  private volatile String agentIdValue;

  @Override
  protected final Integer doCall(@Nonnull final AimClient aimClient) {
    final AgentId agentId = (StringUtils.isEmpty(agentIdValue) ? getMandatoryAgentConfig().getAgentID() : new AgentId(agentIdValue));
    return doRunWith(aimClient, agentId);
  }

  protected abstract Integer doRunWith(@Nonnull AimClient aimClient, @Nonnull AgentId agentId);

}
