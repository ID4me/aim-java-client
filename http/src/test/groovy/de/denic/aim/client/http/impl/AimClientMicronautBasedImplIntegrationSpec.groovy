/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http.impl

import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.RSAKey
import de.denic.aim.client.AgentData
import de.denic.aim.client.AgentId
import de.denic.aim.client.Id4Me
import de.denic.aim.client.http.AimClient
import de.denic.aim.client.http.AimClientException
import de.denic.aim.client.http.AuthorisationChallenge
import de.denic.aim.client.http.AuthzID
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.RxHttpClient
import org.apache.commons.lang3.RandomUtils
import org.spockframework.util.Pair
import spock.lang.Shared
import spock.lang.Specification

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPublicKey

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static de.denic.aim.client.http.AuthzID.of
import static java.lang.Long.parseLong
import static java.time.ZonedDateTime.now
import static java.util.Locale.GERMAN
import static java.util.UUID.randomUUID

class AimClientMicronautBasedImplIntegrationSpec extends Specification {

  private static final File TEST_RESOURCES_FOLDER = new File('./src/test/resources').canonicalFile
  private static final File TEST_JWK_FILE = new File(TEST_RESOURCES_FOLDER, 'testKeyPair.jwk')
  private static final File TEST_AGENT_ID_FILE = new File(TEST_RESOURCES_FOLDER, 'testAgentID.txt')
  private static final File TEST_AUTHORISATION_ID_FILE = new File(TEST_RESOURCES_FOLDER, 'testAuthzIdentifier.txt')
  private static final URI AGENTS_ISSUER_URI = URI.create('https://agent.issuer.uri.test')

  private static AimClient CUT
  private static AgentId idOfKnownAgent
  private static HttpClient httpClient
  @Shared
  private static AuthzID knownAuthorisationId

  def setupSpec() {
    if (!TEST_RESOURCES_FOLDER.isDirectory()) assert TEST_RESOURCES_FOLDER.mkdirs()
    Pair<JWK, AgentId> keyPairAndAgentId = loadAvailableKeyPairAndAgentIdFromFiles()
    JWK keyPair = (keyPairAndAgentId ? keyPairAndAgentId.first() : createAndSaveNewKeyPair())
    idOfKnownAgent = keyPairAndAgentId?.second()
    knownAuthorisationId = loadAuthorisationIdFromFile()
    def httpClient = RxHttpClient.create(new URL('http://irrelevant'))
    CUT = new AimClientMicronautBasedImpl(new URL('https://id.staging.denic.de'), keyPair, httpClient)
    if (!idOfKnownAgent) {
      def agentName = "test_${InetAddress.localHost.hostName}_${new Random().nextInt(99999)}"
      println "Going to register new test agent, name '${agentName}'"
      def agent = CUT.registerAgentWith(agentName, 'Full name of ' + agentName, 'bar-email@test.foo', AGENTS_ISSUER_URI)
      assert agent
      idOfKnownAgent = agent.id
      writeToAgentIdFile(idOfKnownAgent)
    }
  }

  def cleanupSpec() {
    httpClient?.close()
  }

  private Optional<AgentData> agentToBeKnown

  def 'Query for (and later on change) existing Agent'() {
    when:
    agentToBeKnown = CUT.loadDataOfAgentWith(idOfKnownAgent)

    then:
    agentToBeKnown.isPresent()
    idOfKnownAgent == agentToBeKnown.map { agent -> agent.id }.orElse(null)

    /*
    when:
    def knownAgent = agentToBeKnown.get()
    def fullName = knownAgent.fullName
    def changedFullName = (fullName.startsWith(MODIFIED) ? fullName.substring(MODIFIED.length()) : MODIFIED + fullName)
    def modifiedAgent = CUT.updateAgentTo(knownAgent.toModifier().withFullName(changedFullName).build())

    then:
    modifiedAgent != null
    modifiedAgent.fullName == changedFullName
    */
  }

  def 'Querying non-existing Agent'() {
    given:
    def randomAgentId = new AgentId(randomUUID().toString())

    when:
    CUT.loadDataOfAgentWith(randomAgentId).isEmpty()

    then:
    def exception = thrown(AimClientException)
    exception.message ==~ /Lookup of Agent '${randomAgentId}' failed. Response status 'FORBIDDEN'. (.*)/
  }

  def 'Issue and/or query an Identity Authorisation'() {
    given:
    def identityToAuthorise = new Id4Me("test-${RandomUtils.nextInt(1_000, 9_999)}.id4me.identifier")
    final boolean identityAuthorisationNeedsCreation = (knownAuthorisationId == null)
    AuthorisationChallenge identityAuthorisation

    when:
    if (identityAuthorisationNeedsCreation) {
      identityAuthorisation = CUT.issueAuthorisationWith(identityToAuthorise, GERMAN)
      knownAuthorisationId = identityAuthorisation.id
    }

    then:
    if (identityAuthorisationNeedsCreation) {
      assert identityAuthorisation.expirationTS.isAfter(now())
      assert identityAuthorisation.identifier == identityToAuthorise
    }

    when:
    def optIdentityAuthorisation = CUT.loadIdentityAuthorisationWith(knownAuthorisationId)

    then:
    optIdentityAuthorisation.isPresent()
  }

  def 'Query non-existent Identity Authorisation'() {
    given:
    def randomIdentityAuthorisationId = of(RandomUtils.nextLong(9_999_000L, 9_999_999L))

    expect:
    CUT.loadIdentityAuthorisationWith(randomIdentityAuthorisationId).isEmpty()
  }

  def 'Issue DNS setup verification of an Identity Authorisation'() {
    expect:
    knownAuthorisationId

    when:
    CUT.verifyDnsSetupOfIdentityAuthorisation(knownAuthorisationId)

    then:
    def exception = thrown(AimClientException)
    exception.message ==~ /Verification of an (.*)Identity Authorisation '${knownAuthorisationId}' failed(.*)/
  }

  def 'Query non-existent Identity by Subject'() {
    expect:
    CUT.loadIdentityBySubject('unknown.subject').isEmpty()
  }

  def 'Query non-existent Identity by Identifier'() {
    given:
    Id4Me unknownIdentity = new Id4Me('unknown.identifier')

    expect:
    CUT.loadIdentityById(unknownIdentity).isEmpty()
  }

  def 'Deleting identifier from non-existent Subject'() {
    expect:
    !CUT.deletedIdentityFromSubject('unknown.subject', irrelevantIdentity)

    where:
    irrelevantIdentity = new Id4Me('unknown.identifier')
  }

  private static void writeToAgentIdFile(AgentId agentId) {
    if (!TEST_AGENT_ID_FILE.exists()) assert TEST_AGENT_ID_FILE.createNewFile()
    TEST_AGENT_ID_FILE.withWriter('UTF-8') {
      writer -> writer.write(agentId.value)
    }
    println "Written Agent ID to ${TEST_AGENT_ID_FILE}"
  }

  private static Pair<JWK, AgentId> loadAvailableKeyPairAndAgentIdFromFiles() {
    if (!TEST_JWK_FILE.canRead() || !TEST_AGENT_ID_FILE.canRead()) return null

    JWK jwk = TEST_JWK_FILE.withReader('UTF-8') {
      reader -> JWK.parse(reader.text)
    }
    println "Read already setup test JWT from ${TEST_JWK_FILE}"
    AgentId agentId = TEST_AGENT_ID_FILE.withReader('UTF-8') {
      reader -> new AgentId(reader.text)
    }
    println "Read already setup test Agent ID from ${TEST_AGENT_ID_FILE}"
    Pair.of(jwk, agentId)
  }

  private static AuthzID loadAuthorisationIdFromFile() {
    if (!TEST_AUTHORISATION_ID_FILE.canRead()) {
      println "Found NO already setup test Authorisation ID source '${TEST_AUTHORISATION_ID_FILE}'"
      return null
    }

    def authorisationId = TEST_AUTHORISATION_ID_FILE.withReader('UTF-8') {
      reader -> of(parseLong(reader.text.trim()))
    }
    println "Read already setup test Authorisation ID from '${TEST_AUTHORISATION_ID_FILE}'"
    authorisationId
  }

  private static JWK createAndSaveNewKeyPair() {
    KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair()
    final RSAKey rsaKey = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
            .privateKey(keyPair.getPrivate())
            .keyID(randomUUID().toString())
            .keyUse(SIGNATURE)
            .build()
    if (!TEST_JWK_FILE.exists()) assert TEST_JWK_FILE.createNewFile()
    TEST_JWK_FILE.withWriter('UTF-8') { writer -> writer.write(rsaKey.toJSONString()) }
    println "Written test JWT to ${TEST_JWK_FILE}"
    JWK.parse(rsaKey.toJSONObject())
  }

}
