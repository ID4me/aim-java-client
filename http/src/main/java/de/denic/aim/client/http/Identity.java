/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.denic.aim.client.Id4Me;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Locale;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.time.Instant.ofEpochSecond;
import static java.time.ZonedDateTime.ofInstant;
import static java.util.Optional.ofNullable;

@JsonDeserialize(as = Identity.Impl.class)
@JsonSerialize(as = Identity.Impl.class)
public interface Identity {

  @Nonnull
  Id4Me getIdentifier();

  @NotEmpty
  String getSubject();

  @Nonnull
  Optional<Locale> getLocale();

  @Nonnull
  Optional<URI> getPasswordSetupURI();

  @Nonnull
  Optional<ZonedDateTime> getPasswordSetupURLExpirationDate();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @JsonPropertyOrder({"identifier", "sub", "locale", "url", "url_expire"})
  @Immutable
  final class Impl implements Identity {

    private static final ZoneId Z_ZONE = ZoneId.of("Z");

    private final Id4Me identifier;
    private final String subject;
    private final Locale locale;
    private final URI passwordSetupURI;
    private final ZonedDateTime passwordSetupURLExpirationDate;

    public Impl(@Nonnull final Id4Me identifier,
                @Nonnull final String subject,
                @Nullable final Locale locale,
                @Nullable final URI passwordSetupURI,
                @Nullable final ZonedDateTime passwordSetupURLExpirationDate) {
      this.identifier = Validate.notNull(identifier, "Missing ID4me identifier");
      this.subject = Validate.notNull(subject, "Missing subject");
      if (locale != null) {
        Validate.notEmpty(locale.getLanguage(), "Missing language");
      }
      this.locale = locale;
      this.passwordSetupURI = passwordSetupURI;
      this.passwordSetupURLExpirationDate = passwordSetupURLExpirationDate;
    }

    public Impl(@JsonSetter("identifier") @Nonnull final Id4Me identifier,
                @JsonSetter("sub") @Nonnull final String subject,
                @JsonSetter("locale") @Nullable final Locale locale,
                @JsonSetter("url") @Nullable final URI passwordSetupURI,
                @JsonSetter("url_expire") @Nullable final Long passwordSetupURLExpirationEpoch) {
      this(identifier, subject, locale, passwordSetupURI,
              (passwordSetupURLExpirationEpoch == null ?
                      null :
                      ofInstant(ofEpochSecond(passwordSetupURLExpirationEpoch), Z_ZONE)));
    }

    @JsonGetter("identifier")
    @Nonnull
    @Override
    public Id4Me getIdentifier() {
      return identifier;
    }

    @JsonGetter("sub")
    @Override
    public @NotEmpty String getSubject() {
      return subject;
    }

    @JsonIgnore
    @Nonnull
    @Override
    public Optional<Locale> getLocale() {
      return ofNullable(locale);
    }

    @JsonGetter("locale")
    @Nullable
    public String getLanguage() {
      return getLocale().map(Locale::getLanguage).orElse(null);
    }

    @JsonGetter("url")
    @Nonnull
    @Override
    public Optional<URI> getPasswordSetupURI() {
      return ofNullable(passwordSetupURI);
    }

    @JsonIgnore
    @Nonnull
    @Override
    public Optional<ZonedDateTime> getPasswordSetupURLExpirationDate() {
      return ofNullable(passwordSetupURLExpirationDate);
    }

    @JsonGetter("url_expire")
    @Nonnull
    public Optional<Long> getPasswordSetupURLExpirationEpoch() {
      return ofNullable(passwordSetupURLExpirationDate).map(ZonedDateTime::toEpochSecond);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!identifier.equals(impl.identifier))
        return false;
      if (!subject.equals(impl.subject))
        return false;
      if (locale != null ? !locale.equals(impl.locale) : impl.locale != null)
        return false;
      if (passwordSetupURI != null ? !passwordSetupURI.equals(impl.passwordSetupURI) : impl.passwordSetupURI != null)
        return false;
      return passwordSetupURLExpirationDate != null ? passwordSetupURLExpirationDate.equals(impl.passwordSetupURLExpirationDate) : impl.passwordSetupURLExpirationDate == null;
    }

    @Override
    public int hashCode() {
      int result = identifier.hashCode();
      result = 31 * result + subject.hashCode();
      result = 31 * result + (locale != null ? locale.hashCode() : 0);
      result = 31 * result + (passwordSetupURI != null ? passwordSetupURI.hashCode() : 0);
      result = 31 * result + (passwordSetupURLExpirationDate != null ? passwordSetupURLExpirationDate.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "Identity ('" + identifier + '\'' +
              ", subject='" + subject + '\'' +
              ", locale='" + locale + '\'' +
              ", passw. setup URI=" + passwordSetupURI +
              ", passw. setup URI expires=" + passwordSetupURLExpirationDate +
              '}';
    }

  }

}
