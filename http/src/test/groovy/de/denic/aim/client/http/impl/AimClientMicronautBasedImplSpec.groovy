package de.denic.aim.client.http.impl

import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import de.denic.aim.client.Id4Me
import de.denic.aim.client.http.AimClient
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import spock.lang.Specification
import spock.lang.Subject

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static io.micronaut.http.HttpStatus.NOT_FOUND
import static io.micronaut.http.HttpStatus.NO_CONTENT
import static io.micronaut.http.MediaType.TEXT_PLAIN_TYPE
import static java.util.Optional.of

class AimClientMicronautBasedImplSpec extends Specification {

  private static final ECKey AGENT_KEY_PAIR = new ECKeyGenerator(Curve.P_256)
          .keyUse(SIGNATURE)
          .keyID(UUID.randomUUID().toString())
          .generate()
  private static final URL AIM_SERVER_URL = new URL('http://dummy.aim.server')

  HttpClient httpClient = Mock()
  BlockingHttpClient blockingHttpClient = Mock()
  HttpResponse httpResponse = Mock()
  @Subject
  AimClient aimClient = new AimClientMicronautBasedImpl(AIM_SERVER_URL, AGENT_KEY_PAIR, httpClient)

  def setup() {
    _ * httpClient.toBlocking() >> blockingHttpClient
  }

  def 'Successful deletion of known subject/identifier pair'() {
    given:
    HttpRequest<?> httpRequest

    when:
    boolean deleted = aimClient.deletedIdentityFromSubject('known.subject', identifier)

    then:
    1 * blockingHttpClient.exchange(_, String) >> {
      httpRequest = it[0]
      return httpResponse
    }
    1 * httpResponse.status >> NO_CONTENT
    httpRequest.uri.toASCIIString() == "${AIM_SERVER_URL}/aim/v2/subject/known.subject/id/${identifier}"
    httpRequest.methodName == 'DELETE'
    deleted

    where:
    identifier = new Id4Me('id4me')
  }

  def 'No-op deletion of unknown subject/identifier pair'() {
    given:
    HttpRequest<?> httpRequest

    when:
    boolean deleted = aimClient.deletedIdentityFromSubject('unknown.subject', identifier)

    then:
    1 * blockingHttpClient.exchange(_, String) >> {
      httpRequest = it[0]
      throw new HttpClientResponseException('Subject not found', httpResponse)
    }
    1 * httpResponse.contentType >> of(TEXT_PLAIN_TYPE)
    _ * httpResponse.status >> NOT_FOUND
    httpRequest.uri.toASCIIString() == "${AIM_SERVER_URL}/aim/v2/subject/unknown.subject/id/${identifier}"
    httpRequest.methodName == 'DELETE'
    !deleted

    where:
    identifier = new Id4Me('id4me')
  }

}
