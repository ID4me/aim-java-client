/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business.impl;

import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.business.DnsSetupException;
import de.denic.aim.client.business.DnsSetupSPI;
import de.denic.aim.client.business.Identity;
import de.denic.aim.client.business.IdentityAuthorisation.DnsSetup;
import de.denic.aim.client.http.Challenge;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.lang.Thread.currentThread;
import static java.nio.charset.Charset.defaultCharset;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.join;
import static org.xbill.DNS.DClass.IN;
import static org.xbill.DNS.Message.newQuery;
import static org.xbill.DNS.Rcode.NOERROR;
import static org.xbill.DNS.Type.TXT;

@ThreadSafe
public final class DnsSetupViaGCloudCommand implements DnsSetupSPI {

  public static final int SPI_PRIORITY = 100;

  private static final Logger LOG = LoggerFactory.getLogger(DnsSetupViaGCloudCommand.class);
  private static final Duration ONE_SECOND = Duration.ofSeconds(1L);
  private static final Duration TEN_SECONDS = ONE_SECOND.multipliedBy(10L);
  private static final Duration ASSUMED_DNS_QUERY_LATENCY = Duration.ofMillis(50L);
  private static final Name ACME_CHALLENGE = Name.fromConstantString("_acme-challenge");
  private static final Pattern END_OF_INPUT_LINE_BREAK = Pattern.compile("\\v\\z");

  private final Object gCloudManagedZoneNamesAndDnsNamesGuard = new Object();
  @GuardedBy("gCloudManagedZoneNamesAndDnsNamesGuard")
  private Collection<? extends Pair<String, Name>> gCloudManagedZoneNamesAndDnsNames;

  @Override
  public int getPriority() {
    return SPI_PRIORITY;
  }

  @Override
  public boolean accountableFor(@Nonnull final Challenge challenge) {
    if (!checkForExecutableGCloudCommand())
      return false;

    synchronized (gCloudManagedZoneNamesAndDnsNamesGuard) {
      gCloudManagedZoneNamesAndDnsNames = loadMapOfGCloudNamesToDnsNames();
      return isPartOfGCloudManagedZones(challenge.getIdentifier());
    }
  }

  private static List<? extends Pair<String, Name>> loadMapOfGCloudNamesToDnsNames() {
    /*
    $ gcloud dns managed-zones list
    NAME        DNS_NAME     DESCRIPTION  VISIBILITY
    mydenic-de  mydenic.de.               public
    */
    final Pair<String, String> codeSysOutAndErr = executeGCloudCommand("Evaluate DNS cloud managed zones",
            5L, "gcloud", "dns", "managed-zones", "list");
    final String[] sysOutLines = codeSysOutAndErr.getLeft().split("\\v+");
    if (sysOutLines.length < 2)
      return emptyList();

    return stream(sysOutLines)
            .skip(1L) // Line with headers
            .map(line -> line.split("\\s+"))
            .map(lineElements -> new ImmutablePair<>(lineElements[0], Name.fromConstantString(lineElements[1])))
            .collect(toList());
  }

  private boolean isPartOfGCloudManagedZones(final Id4Me identity) {
    final String identitiesFQDnsName = Validate.notNull(identity, "Missing identity").toFullyQualifiedDnsName();
    synchronized (gCloudManagedZoneNamesAndDnsNamesGuard) {
      return gCloudManagedZoneNamesAndDnsNames.stream()
              .map(Pair::getRight)
              .map(Name::toString)
              .anyMatch(identitiesFQDnsName::endsWith);
    }
  }

  @Nonnull
  @Override
  public DnsSetup setup(@Nonnull final Challenge challenge,
                        @Nonnull final JWK keyPair,
                        @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) throws DnsSetupException {
    final Id4Me identifier = Validate.notNull(challenge, "Missing ACME challenge").getIdentifier();
    final String acmeChallengeRecordData = challenge.computeACMEChallengeValueWith(keyPair);
    final String gCloudZone = findGCloudManagedZoneMatching(identifier);
    try {
      startGCloudDnsRecordSetTransactionTargetting(gCloudZone);
      executeGCloudCommand("Adding '_acme-challenge' TXT record to gcloud dns record-set transaction", 3L,
              "gcloud", "dns", "record-sets", "transaction", "add",
              "--zone=" + gCloudZone,
              "--name=_acme-challenge." + identifier.toFullyQualifiedDnsName(),
              "--ttl", "300",
              "--type", "TXT",
              "\"" + acmeChallengeRecordData + "\"");
      executeGCloudDnsRecordSetTransactionTargetting(gCloudZone);
      return new DnsSetupImpl(challenge, gCloudZone, acmeChallengeRecordData, dnsSetupVerificationCallback);
    } catch (final RuntimeException e) {
      abortGCloudDnsRecordSetTransactionTargetting(gCloudZone);
      throw e;
    }
  }

  private static void startGCloudDnsRecordSetTransactionTargetting(final String gCloudZone) {
    executeGCloudCommand("Start gcloud dns record-set transaction", 3L,
            "gcloud", "dns", "record-sets", "transaction", "start", "--zone=" + gCloudZone);
  }

  private static void executeGCloudDnsRecordSetTransactionTargetting(final String gCloudZone) {
    executeGCloudCommand("Executing gcloud dns record-set transaction", 12L,
            "gcloud", "dns", "record-sets", "transaction", "execute", "--zone=" + gCloudZone);
  }

  private static void abortGCloudDnsRecordSetTransactionTargetting(final String gCloudZone) {
    if (!sucessfullyExecutedGCloudCommand("Aborting gcloud dns record-set transaction", 1L,
            "gcloud", "dns", "record-sets", "transaction", "abort", "--zone=" + gCloudZone)) {
      LOG.warn("Cleanup of gcloud DNS record-set transaction failed!");
    }
  }

  @Nonnull
  private String findGCloudManagedZoneMatching(@Nonnull final Id4Me identity) {
    final Name identityName = Name.fromConstantString(identity.toFullyQualifiedDnsName());
    synchronized (gCloudManagedZoneNamesAndDnsNamesGuard) {
      return gCloudManagedZoneNamesAndDnsNames.stream()
              .filter(nameDnsNamePair -> identityName.subdomain(nameDnsNamePair.getRight()))
              .map(Pair::getLeft)
              .findFirst()
              .orElseThrow(() -> new RuntimeException("Found no Google DNS Zone responsible for ID4me '" + identity + "'"));
    }
  }

  private static boolean checkForExecutableGCloudCommand() {
    return sucessfullyExecutedGCloudCommand("Availability-check of gcloud command", 2L,
            "gcloud", "--version");
  }

  private static boolean sucessfullyExecutedGCloudCommand(final String purpose, final long timeoutSeconds,
                                                          final String... gCloudCommandParams) {
    try {
      executeGCloudCommand(purpose, timeoutSeconds, gCloudCommandParams);
      return true;
    } catch (final DnsSetupException e) {
      return false;
    }
  }

  /**
   * @return {@link Pair} contains SYSOUT and SYSERR data of gcloud command execution.
   */
  private static Pair<String, String> executeGCloudCommand(final String purpose, final long timeoutSeconds,
                                                           final String... gCloudCommandParams) throws DnsSetupException {
    try {
      final Process gCloudProcess = new ProcessBuilder().command(gCloudCommandParams).start();
      final boolean gCloudProcessHasFinished = gCloudProcess.waitFor(timeoutSeconds, SECONDS);
      if (gCloudProcessHasFinished) {
        final String sysOut = RegExUtils.removeAll(
                IOUtils.toString(gCloudProcess.getInputStream(), defaultCharset()),
                END_OF_INPUT_LINE_BREAK);
        final String sysErr = RegExUtils.removeAll(IOUtils.toString(gCloudProcess.getErrorStream(), defaultCharset()),
                END_OF_INPUT_LINE_BREAK);
        final int exitCode = gCloudProcess.exitValue();
        LOG.debug("{} via '{}' yields: Exit code {}; SYSOUT >{}<; SYSERR >{}<",
                purpose, join(gCloudCommandParams, ' '), exitCode, sysOut, sysErr);
        if (exitCode != 0) {
          throw new DnsSetupException(purpose + " failed! Exit code: " + exitCode + ". SYSERR: >" + sysErr + "<");
        }

        return new ImmutablePair<>(sysOut, sysErr);
      }

      throw new DnsSetupException("Execution of command '" + join(gCloudCommandParams, ' ') +
              "' has NOT finished within " + timeoutSeconds + " seconds!");
    } catch (final InterruptedException e) {
      throw new DnsSetupException("Execution of command '" + join(gCloudCommandParams, ' ') + "' has been INTERRUPTED!");
    } catch (final IOException e) {
      throw new DnsSetupException("Execution of command '" + join(gCloudCommandParams, ' ') + "' has FAILED!", e);
    }
  }

  @Immutable
  private static final class DnsSetupImpl implements DnsSetup {

    private static final Duration HALF_A_SECOND = Duration.ofMillis(500L);

    private final Challenge challenge;
    private final String gCloudZone;
    private final String acmeChallengeRecordData;
    private final DnsSetupVerificationCallback dnsSetupVerificationCallback;

    private DnsSetupImpl(@Nonnull final Challenge challenge,
                         @NotEmpty final String gCloudZone,
                         @NotEmpty final String acmeChallengeRecordData,
                         @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) {
      this.challenge = Validate.notNull(challenge, "Missing ACME challenge");
      this.gCloudZone = Validate.notEmpty(gCloudZone, "Missing gcloud zone");
      this.acmeChallengeRecordData = Validate.notEmpty(acmeChallengeRecordData, "Missing ACME challenge record data");
      this.dnsSetupVerificationCallback = Validate.notNull(dnsSetupVerificationCallback,
              "Missing DNS setup verification callback");
    }

    @Nonnull
    @Override
    public Optional<Checked> pollDnsSetupPropagationAt(@Nullable final InetAddress resolverAddress, @Nonnull final Duration timeout) {
      Validate.isTrue(!Validate.notNull(timeout, "Missing timeout").isNegative(), "Negative timeout not allowed");
      final Duration queryInterval;
      int queryCount;
      if (timeout.compareTo(TEN_SECONDS) > 0) {
        Validate.isTrue(timeout.compareTo(MAX_TIMEOUT) <= 0, "Timeout to long: %s > %s", timeout, MAX_TIMEOUT);
        queryInterval = timeout.dividedBy(10L).minus(ASSUMED_DNS_QUERY_LATENCY);
        queryCount = 10;
      } else {
        queryCount = (int) Long.max(timeout.dividedBy(ONE_SECOND), 1L);
        queryInterval = ONE_SECOND.minus(ASSUMED_DNS_QUERY_LATENCY);
      }

      final SimpleResolver resolver;
      try {
        resolver = new SimpleResolver();
      } catch (final UnknownHostException e) {
        throw new RuntimeException("Internal error setting up DNS resolver", e);
      }

      resolver.setAddress(Validate.notNull(resolverAddress, "Missing address of DNS resolver"));
      resolver.setTimeout(HALF_A_SECOND);
      final Message acmeChallengeRecordQuery = prepareQueryForAcmeChallengeOf();
      try {
        do
        {
          Thread.sleep(queryInterval.toMillis());
          try {
            final Message acmeChallengeResponse = resolver.send(acmeChallengeRecordQuery);
            final int responseRCode = acmeChallengeResponse.getHeader().getRcode();
            if (NOERROR == responseRCode) {
              LOG.info("Sucessfully queried for ACME challenge resource record of '{}'",
                      challenge.getIdentifier());
              return afterSuccessfulQueryOfLocalResolverFor(acmeChallengeRecordQuery, queryInterval);
            }
            LOG.debug("Querying for ACME challenge resource record of '{}' failed: {}",
                    challenge.getIdentifier(), Rcode.string(responseRCode));
          } catch (final IOException e) {
            LOG.warn("Querying for ACME challenge resource record of '{}' failed",
                    challenge.getIdentifier(), e);
          }
        } while (--queryCount > 0);
        LOG.warn("Querying for ACME challenge resource record of '{}' timed out",
                challenge.getIdentifier());
      } catch (final InterruptedException e) {
        LOG.warn("Interruption of {}", this, e);
        currentThread().interrupt();
      }
      return empty();
    }

    private Optional<Checked> afterSuccessfulQueryOfLocalResolverFor(@Nonnull final Message acmeChallengeRecordQuery,
                                                                     @Nonnull final Duration queryInterval) {
      final SimpleResolver localResolver;
      try {
        localResolver = new SimpleResolver();
      } catch (final UnknownHostException e) {
        throw new RuntimeException("Internal error setting up local DNS resolver", e);
      }

      localResolver.setTimeout(HALF_A_SECOND);
      try {
        Thread.sleep(queryInterval.toMillis());
        try {
          final Message acmeChallengeResponse = localResolver.send(acmeChallengeRecordQuery);
          final int responseRCode = acmeChallengeResponse.getHeader().getRcode();
          if (NOERROR == responseRCode) {
            LOG.debug("Sucessfully queried local resolver for ACME challenge resource record of '{}'",
                    challenge.getIdentifier());
            final Optional<Checked> result = of(new DnsSetupCheckedImpl(challenge, gCloudZone,
                    acmeChallengeRecordData, dnsSetupVerificationCallback));
            Thread.sleep(queryInterval.toMillis()); // Just to be much more sure that also AIM server sees it!
            return result;
          }
          LOG.info("Querying local resolver for ACME challenge resource record of '{}' failed: {}",
                  challenge.getIdentifier(), Rcode.string(responseRCode));
        } catch (final IOException e) {
          LOG.warn("Querying local resolver for ACME challenge resource record of '{}' failed",
                  challenge.getIdentifier(), e);
        }
      } catch (final InterruptedException e) {
        LOG.warn("Interruption of {}", this, e);
        currentThread().interrupt();
      }
      return empty();
    }

    private Message prepareQueryForAcmeChallengeOf() {
      final Name acmeChallengeName;
      try {
        acmeChallengeName = Name.concatenate(ACME_CHALLENGE,
                Name.fromConstantString(challenge.getIdentifier().toFullyQualifiedDnsName()));
      } catch (final NameTooLongException e) {
        throw new RuntimeException("Internal error preparing ACME challenge query DNS message", e);
      }

      return newQuery(TXTRecord.newRecord(acmeChallengeName, TXT, IN));
    }

    @Override
    public String toString() {
      return "DNS setup of " + challenge;
    }

  }

  @Immutable
  private static class DnsSetupCheckedImpl implements DnsSetup.Checked {

    private final Challenge challenge;
    private final String gCloudZone;
    private final String acmeChallengeRecordData;
    private final DnsSetupVerificationCallback dnsSetupVerificationCallback;

    private DnsSetupCheckedImpl(@Nonnull final Challenge challenge,
                                @NotEmpty final String gCloudZone,
                                @NotEmpty final String acmeChallengeRecordData,
                                @Nonnull final DnsSetupVerificationCallback dnsSetupVerificationCallback) {
      this.challenge = Validate.notNull(challenge, "Missing ACME challenge");
      this.gCloudZone = Validate.notEmpty(gCloudZone, "Missing gcloud zone");
      this.acmeChallengeRecordData = Validate.notEmpty(acmeChallengeRecordData, "Missing ACME challenge record data");
      this.dnsSetupVerificationCallback = Validate.notNull(dnsSetupVerificationCallback,
              "Missing DNS setup verification callback");
    }

    @Nonnull
    @Override
    public Identity issueDnsSetupVerification() {
      final Identity verifiedIdentity = dnsSetupVerificationCallback.verifyDnsSetup();
      setupOpenIDAndRemoveAcmeChallengeDNSRecord();
      return verifiedIdentity;
    }

    private void setupOpenIDAndRemoveAcmeChallengeDNSRecord() throws DnsSetupException {
      final Id4Me identifier = challenge.getIdentifier();
      try {
        startGCloudDnsRecordSetTransactionTargetting(gCloudZone);
        executeGCloudCommand("Adding '_openid' TXT record to gcloud dns record-set transaction", 3L,
                "gcloud", "dns", "record-sets", "transaction", "add",
                "--zone=" + gCloudZone,
                "--name=_openid." + identifier.toFullyQualifiedDnsName(),
                "--ttl", "3600",
                "--type", "TXT",
                "\"v=OID1;iss=id.staging.denic.de\"");
        executeGCloudCommand("Removing '_acme-challenge' TXT record to gcloud dns record-set transaction", 3L,
                "gcloud", "dns", "record-sets", "transaction", "remove",
                "--zone=" + gCloudZone,
                "--name=_acme-challenge." + identifier.toFullyQualifiedDnsName(),
                "--ttl", "300",
                "--type", "TXT",
                "\"" + acmeChallengeRecordData + "\"");
        executeGCloudDnsRecordSetTransactionTargetting(gCloudZone);
      } catch (final RuntimeException e) {
        abortGCloudDnsRecordSetTransactionTargetting(gCloudZone);
        throw e;
      }
    }

    @Override
    public String toString() {
      return "Checked for propagation of ACME challenge record DNS setup of " + challenge;
    }

  }

}
