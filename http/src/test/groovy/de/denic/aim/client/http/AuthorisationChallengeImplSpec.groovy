/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import de.denic.aim.client.Id4Me
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime

import static AuthzID.of
import static java.util.Locale.ENGLISH
import static java.util.Locale.GERMAN

class AuthorisationChallengeImplSpec extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
          .registerModule(new Jdk8Module())
  private static final AuthzID ID = of(42)
  private static final Id4Me IDENTIFIER = new Id4Me('id.4me.identifier')
  private static final ZoneId Z_ZONE = ZoneId.of('Z')
  private static final ZonedDateTime EXPIRATION_DATE = ZonedDateTime.of(2100, 4, 11, 19, 45, 3, 0, Z_ZONE)
  private static final ZonedDateTime EXPIRATION_DATE_WITH_MS = ZonedDateTime.of(2100, 4, 11, 19, 45, 3, 123000000, Z_ZONE)
  private static final ZonedDateTime VALIDATION_TS = ZonedDateTime.of(2090, 3, 21, 1, 0, 59, 0, Z_ZONE)
  private static final ZonedDateTime VALIDATION_TS_WITH_MS = ZonedDateTime.of(2090, 3, 21, 1, 0, 59, 987000000, Z_ZONE)
  private static final String TOKEN = 'ACME-dns-challenge-token'

  def 'Proper JSON serialization/deserialization'() {
    expect:
    jsonSerialized == expectedJSON
    OBJECT_MAPPER.readerFor(AuthorisationChallenge).readValue(jsonSerialized) == toSerialize

    where:
    id | identifier | expirationTS            | token | validationTS          | language || expectedJSON
    ID | IDENTIFIER | EXPIRATION_DATE         | TOKEN | null                  | GERMAN   || '{"identifier_authz_id":42,' +
            '"identifier":"id.4me.identifier","expiration_ts":"2100-04-11T19:45:03Z","token":"ACME-dns-challenge-token","locale":"de"}'
    ID | IDENTIFIER | EXPIRATION_DATE         | TOKEN | null                  | GERMAN   || '{"identifier_authz_id":42,' +
            '"identifier":"id.4me.identifier","expiration_ts":"2100-04-11T19:45:03Z","token":"ACME-dns-challenge-token","locale":"de"}'
    ID | IDENTIFIER | EXPIRATION_DATE         | TOKEN | VALIDATION_TS         | ENGLISH  || '{"identifier_authz_id":42,' +
            '"identifier":"id.4me.identifier","expiration_ts":"2100-04-11T19:45:03Z","validation_ts":"2090-03-21T01:00:59Z",' +
            '"token":"ACME-dns-challenge-token","locale":"en"}'
    ID | IDENTIFIER | EXPIRATION_DATE_WITH_MS | TOKEN | VALIDATION_TS_WITH_MS | ENGLISH  || '{"identifier_authz_id":42,' +
            '"identifier":"id.4me.identifier","expiration_ts":"2100-04-11T19:45:03.123Z","validation_ts":"2090-03-21T01:00:59.987Z",' +
            '"token":"ACME-dns-challenge-token","locale":"en"}'
    toSerialize = new AuthorisationChallenge.Impl(id, identifier, expirationTS as ZonedDateTime,
            validationTS as ZonedDateTime, token, language)
    jsonSerialized = OBJECT_MAPPER.writerFor(AuthorisationChallenge).writeValueAsString(toSerialize)
  }

}
