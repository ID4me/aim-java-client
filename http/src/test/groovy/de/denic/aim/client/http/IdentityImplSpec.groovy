/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import de.denic.aim.client.Id4Me
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime

import static java.util.Locale.GERMAN
import static java.util.Locale.GERMANY

class IdentityImplSpec extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
          .registerModule(new Jdk8Module())
  private static final URI PASSWORD_SETUP_URL = URI.create('http://password.setup/url')
  private static final Id4Me IDENTIFIER = new Id4Me('id.4me.identifier')
  private static final String SUBJECT = 'id.4me.subject'
  private static final ZoneId Z_ZONE = ZoneId.of('Z')
  private static final ZonedDateTime PASSWORD_SETUP_URL_EXPIRE_DATE = ZonedDateTime.of(2100, 4, 11, 19, 45, 3, 0, Z_ZONE)

  def 'Proper JSON serialization/deserialization'() {
    expect:
    jsonSerialized == expectedJSON
    OBJECT_MAPPER.readerFor(Identity).readValue(jsonSerialized) == toSerialize

    where:
    identifier | subject | language | passwordUrl        | passwordUrlExpiration          || expectedJSON
    IDENTIFIER | SUBJECT | GERMAN   | PASSWORD_SETUP_URL | PASSWORD_SETUP_URL_EXPIRE_DATE || '{"identifier":"id.4me.identifier","sub":"id.4me.subject","locale":"de","url":"http://password.setup/url","url_expire":4111155903}'
    IDENTIFIER | SUBJECT | null     | null               | null                           || '{"identifier":"id.4me.identifier","sub":"id.4me.subject"}'
    toSerialize = new Identity.Impl(identifier, subject, language, passwordUrl, passwordUrlExpiration)
    jsonSerialized = OBJECT_MAPPER.writerFor(Identity).writeValueAsString(toSerialize)
  }

  def 'Proper JSON serialization'() {
    expect:
    OBJECT_MAPPER.writerFor(Identity).writeValueAsString(toSerialize) == expectedJSON

    where:
    identifier | subject | language | passwordUrl | passwordUrlExpiration || expectedJSON
    IDENTIFIER | SUBJECT | GERMANY  | null        | null                  || '{"identifier":"id.4me.identifier","sub":"id.4me.subject","locale":"de"}'
    toSerialize = new Identity.Impl(identifier, subject, language, passwordUrl, passwordUrlExpiration)
  }

}
