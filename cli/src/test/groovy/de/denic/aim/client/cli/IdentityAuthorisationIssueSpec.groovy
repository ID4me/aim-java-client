/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli

import com.nimbusds.jose.jwk.JWK
import de.denic.aim.client.Id4Me
import de.denic.aim.client.http.AuthorisationChallenge
import spock.lang.Specification

import static de.denic.aim.client.cli.IdentityAuthorisationIssue.prepareACMEChallengeTxtRecordFrom
import static de.denic.aim.client.http.AuthzID.of
import static java.nio.charset.StandardCharsets.UTF_8
import static java.time.ZonedDateTime.now
import static java.util.Locale.GERMAN
import static org.apache.commons.io.FileUtils.readFileToString

class IdentityAuthorisationIssueSpec extends Specification {

  def 'Compute some ACME challenge TXT record'() {
    given:
    def authorisation = new AuthorisationChallenge.Impl(of(42), testId4meIdentifier, now(), now(),
            'dnsChallengeToken', GERMAN)
    JWK keyPair = JWK.parse(keyPairJwkFileContent)

    expect:
    prepareACMEChallengeTxtRecordFrom(authorisation, keyPair) ==
            '_acme-challenge.test.id4me.identifier. 300 IN TXT "ed6vMPQKa6TxPzWHg15NWpUuUt3xWbw58HskT_J_bhM"'

    where:
    keyPairJwkFileContent = readFileToString(new File("./src/test/resources/issueIdentifierAuthorisationSpec.test.jwk"), UTF_8)
    testId4meIdentifier = new Id4Me('test.id4me.identifier')
  }

}
