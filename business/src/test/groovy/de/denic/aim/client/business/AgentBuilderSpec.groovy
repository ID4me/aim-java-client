/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business

import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import de.denic.aim.client.AgentId
import de.denic.aim.client.Id4Me
import de.denic.aim.client.business.Agent.Builder.AgentRegisteringCallback
import de.denic.aim.client.business.IdentityAuthorisation.DnsSetup
import de.denic.aim.client.http.AimClientException
import spock.lang.Ignore
import spock.lang.Specification

import javax.annotation.Nonnull
import java.time.Duration

import static java.lang.System.currentTimeMillis
import static java.nio.charset.StandardCharsets.UTF_8
import static java.util.Locale.GERMAN
import static org.apache.commons.io.FileUtils.readFileToString
import static org.apache.commons.io.FileUtils.write

/**
 * Mainly to drive development of the {@link Agent.Builder}/{@link Agent} APIs and to document usage of it.
 */
class AgentBuilderSpec extends Specification implements AgentRegisteringCallback {

  private static final File AGENT_BUILDER_TEST_KEY_PAIR = new File('./src/test/resources/agentBuilderTestKeyPair.jwk')
  private static final URI STAGING_TARGET_URI = URI.create('https://id.staging.denic.de')

  @Ignore('To get executed manually in your IDE')
  def 'registerNewAgentWithNewKeyPair'() {
    when:
    Agent agent = Agent.Builder
            .toRegisterAt(STAGING_TARGET_URI)
            .withNewlyGeneratedRSAKeyPairOfSize(4096)
            .andName('agent-builder-test-2')
            .andFullName('Test Agent created by Agent Builder test case')
            .andEmail('mail@agent.test')
            .andIssuerURI(URI.create('https://issuer.test'))
            .andCallbackTo(this)
    println "Got from builder: $agent"

    then:
    agent
  }

  @Ignore('To get executed manually in your IDE')
  def 'registerNewAgentWithProvidedKeyPair'() {
    given:
    RSAKey keyPair = new RSAKeyGenerator(3072).generate()

    when:
    Agent agent = Agent.Builder
            .toRegisterAt(STAGING_TARGET_URI)
            .withGiven(keyPair)
            .andName('agent-builder-test-2')
            .andFullName('Test Agent created by Agent Builder test case')
            .andEmail('mail@agent.test')
            .andIssuerURI(URI.create('https://issuer.test'))
            .andCallbackTo(this)
    println "Got from builder: $agent"

    then:
    agent
  }

  @Ignore('To get executed manually in your IDE')
  def 'connectAgentWithProvidedKeyPair'() {
    given:
    File keyPairJwkFile = AGENT_BUILDER_TEST_KEY_PAIR.getCanonicalFile()
    String keyPairJwkFileContent = readFileToString(keyPairJwkFile, UTF_8)
    JWK keyPair = JWK.parse(keyPairJwkFileContent)
    Agent agent = Agent.Builder
            .alreadyRegisteredAt(STAGING_TARGET_URI)
            .withKeyPair(keyPair);
    println "Got from builder: $agent"

    expect:
    try {
      IdentityAuthorisation issuedIdentityAuthorisation = agent.issueAuthorisationWith(
              new Id4Me('foo.bar.identifier'), GERMAN)
      println "Issued: $issuedIdentityAuthorisation"
      IdentityAuthorisation reloaded = issuedIdentityAuthorisation.reload()
      println "Reloaded: $reloaded"
      reloaded
    } catch (AimClientException mightHappen) {
      mightHappen.getMessage() == "Issuance of an Identity Authorisation failed. HTTP response status 'CONFLICT'. Message: 'Another ACME challenge for this identity is currently pending.'"
    }
  }

  @Ignore('To get executed manually in your IDE')
  def 'completeID4meIdentifierSetupWorkflow'() {
    given:
    File keyPairJwkFile = AGENT_BUILDER_TEST_KEY_PAIR.getCanonicalFile()
    String keyPairJwkFileContent = readFileToString(keyPairJwkFile, UTF_8)
    JWK keyPair = JWK.parse(keyPairJwkFileContent);
    Agent agent = Agent.Builder
            .alreadyRegisteredAt(STAGING_TARGET_URI)
            .withKeyPair(keyPair)
    IdentityAuthorisation issuedIdentityAuthorisation = agent.issueAuthorisationWith(
            new Id4Me("aim-java-client-test-${currentTimeMillis() / 60_000L}.mydenic.de"),
            GERMAN)

    when:
    Optional<DnsSetup> dnsSetup = issuedIdentityAuthorisation.initiateDnsSetup()

    then:
    dnsSetup.isPresent()
    println "DNS setup: ${dnsSetup.get()}"

    when:
    Optional<DnsSetup.Checked> dnsSetupChecked =
            dnsSetup.get().pollDnsSetupPropagationFor(Duration.ofMinutes(3L))

    then:
    dnsSetupChecked.isPresent()
    println "DNS setup checked: ${dnsSetupChecked.get()}"
    Identity verifiedIdentity = dnsSetupChecked.get().issueDnsSetupVerification()
    println "Verified: $verifiedIdentity"

// AIMv2 currently does not support loading of Identifier data!
//    when:
//    def reloadedIdentity = verifiedIdentity.reload()
//
//    then:
//    reloadedIdentity.isPresent()
//    reloadedIdentity.get() == verifiedIdentity
//    println "Reloaded: ${reloadedIdentity.get()}"

//    try {
//      println "Triggered: ${verifiedIdentity.triggerPasswordSetup()}"
//    } catch (final AimClientException expected) {
//      println "Usually triggering password setup via Agent is forbidden: $expected"
//    }

    cleanup:
    verifiedIdentity?.delete()
  }

  @Override
  void agentRegisteredWith(@Nonnull final AgentId agentId,
                           @Nonnull final Optional<String> status,
                           @Nonnull final RSAKey keyPair) {
    println 'Agent registering callback receives:'
    println "- ID of registered Agent: '$agentId'"
    println "- Status: '${status.orElse("<EMPTY>")}'"
    println "- Key pair: '${keyPair.toJSONString()}'"
    try {
      write(AGENT_BUILDER_TEST_KEY_PAIR.getCanonicalFile(), keyPair.toJSONString(), UTF_8)
    } catch (final IOException e) {
      System.err.println("Writing Key Pair to file $AGENT_BUILDER_TEST_KEY_PAIR failed: $e")
    }
  }

}