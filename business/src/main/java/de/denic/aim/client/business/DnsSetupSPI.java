/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business;

import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.business.IdentityAuthorisation.DnsSetup;
import de.denic.aim.client.http.Challenge;

import javax.annotation.Nonnull;

public interface DnsSetupSPI extends Comparable<DnsSetupSPI> {

  @Override
  default int compareTo(final DnsSetupSPI other) {
    return Integer.compare(getPriority(), other.getPriority());
  }

  /**
   * If there are more than one SPI implementation {@link #accountableFor(Challenge)} a challenge/identity, the one with
   * the highest priority is chosen (so {@link Integer#MAX_VALUE} is maximum priority). For instance, if two
   * implementations are able to operate on Google Cloud DNS, one uses local installed <code>gcloud</code> CLI, the
   * other one Google's REST API, you are able to prioritize the latter one.
   *
   * @see #compareTo(DnsSetupSPI)
   */
  int getPriority();

  boolean accountableFor(@Nonnull Challenge challenge);

  @Nonnull
  DnsSetup setup(@Nonnull Challenge challenge,
                 @Nonnull JWK keyPair,
                 @Nonnull DnsSetupVerificationCallback dnsSetupVerificationCallback) throws DnsSetupException;

  interface DnsSetupVerificationCallback {

    @Nonnull
    Identity verifyDnsSetup() throws DnsSetupException;

  }

}
