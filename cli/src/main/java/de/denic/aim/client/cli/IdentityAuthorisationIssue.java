/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.http.AimClient;
import de.denic.aim.client.http.AuthorisationChallenge;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import javax.validation.constraints.NotEmpty;
import java.util.Locale;

import static de.denic.aim.client.cli.Command.SUCCESS;

@Command(name = "authz-issue", aliases = {"zi"},
        description = "Issues an Authorisation of a new ID4me Identity.")
@ThreadSafe
public final class IdentityAuthorisationIssue extends IdentityRequiringSubCommand {

  @Option(names = {"-l", "--locale"},
          description = {"Two-character ISO code of language to use with the ID4me Identity.",
                  "- Default: '${DEFAULT-VALUE}'"},
          paramLabel = "<LANGUAGE>")
  private volatile String language = Locale.getDefault().getLanguage();

  @Override
  protected Integer doRunWith(@Nonnull final AimClient aimClient,
                              @Nonnull final Id4Me id4me) {
    final Locale locale = Locale.forLanguageTag(language);
    /*final IdentityAuthorisation identityAuthorisation = */
    aimClient.issueAuthorisationWith(id4me, locale);
//    writeToFile(IDENTITY_AUTHZ_FILE, identityAuthorisation.getId().toString(),
//            "ID of newly created Identity Authorisation instance");
//    final String acmeChallengeTxtRecord = prepareAcmeChallengeTxtRecordFrom(id4me, identityAuthorisation, getKeyPair());
//    LOG.info("DNS ACME Challenge resource record to add to your zone file: '{}'", acmeChallengeTxtRecord);
//    writeToIdentifierSpecificFile("_acme-challenge.", id4me, ".txt",
//            acmeChallengeTxtRecord, "DNS ACME Challenge resource record");
    return SUCCESS;
  }

  @NotEmpty
  public static String prepareACMEChallengeTxtRecordFrom(@Nonnull final AuthorisationChallenge authorisation,
                                                         @Nonnull final JWK accountKey) {
    final StringBuilder builder = new StringBuilder();
    builder.append("_acme-challenge.").append(authorisation.getIdentifier().toFullyQualifiedDnsName());
    builder.append(' ');
    builder.append("300");
    builder.append(' ');
    builder.append("IN");
    builder.append(' ');
    builder.append("TXT");
    builder.append(' ');
    builder.append('"').append(authorisation.computeACMEChallengeValueWith(accountKey)).append('"');
    return builder.toString();
  }

}
