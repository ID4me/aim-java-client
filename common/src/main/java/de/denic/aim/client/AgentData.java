/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.aim.client.AgentData.JsonKeys.*;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@JsonDeserialize(as = AgentData.Impl.class)
@JsonSerialize(as = AgentData.Impl.class)
public interface AgentData {

  @Nonnull
  AgentId getId();

  @Nonnull
  Optional<String> getStatus();

  @Nonnull
  String getName();

  @Nonnull
  String getFullName();

  @Nonnull
  String getEmail();

  @Nonnull
  URI getIssuerURI();

  @Nonnull
  Modifier toModifier();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @JsonPropertyOrder({ID, NAME, FULL_NAME, STATUS, EMAIL, ISSUER_URL})
  @Immutable
  final class Impl implements AgentData {

    private final String name;
    private final String fullName;
    private final String email;
    private final AgentId id;
    private final String status;
    private final URI issuerURI;

    public Impl(@JsonSetter(ID) @Nonnull final AgentId id,
                @JsonSetter(STATUS) @Nullable final String status,
                @JsonSetter(NAME) @Nonnull final String name,
                @JsonSetter(FULL_NAME) @Nonnull final String fullName,
                @JsonSetter(EMAIL) @Nonnull final String email,
                @JsonSetter(ISSUER_URL) @Nonnull final URI issuerURI) {
      this.id = Validate.notNull(id, "Missing ID");
      this.name = Validate.notEmpty(name, "Missing name");
      this.fullName = Validate.notEmpty(fullName, "Missing full name");
      this.email = Validate.notEmpty(email, "Missing email");
      this.issuerURI = Validate.notNull(issuerURI, "Missing issuer URI");
      this.status = status;
    }

    @JsonGetter(ID)
    @Override
    @Nonnull
    public AgentId getId() {
      return id;
    }

    @JsonGetter(STATUS)
    @Override
    @Nonnull
    public Optional<String> getStatus() {
      return ofNullable(status);
    }

    @JsonGetter(NAME)
    @Override
    public String getName() {
      return name;
    }

    @JsonGetter(FULL_NAME)
    @Override
    public String getFullName() {
      return fullName;
    }

    @JsonGetter(EMAIL)
    @Override
    public String getEmail() {
      return email;
    }

    @JsonGetter(ISSUER_URL)
    @Override
    public URI getIssuerURI() {
      return issuerURI;
    }

    @Nonnull
    @Override
    public Modifier toModifier() {
      return new Modifier.Impl(this);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!name.equals(impl.name))
        return false;
      if (!fullName.equals(impl.fullName))
        return false;
      if (!email.equals(impl.email))
        return false;
      if (!id.equals(impl.id))
        return false;
      if (status != null ? !status.equals(impl.status) : impl.status != null)
        return false;
      return issuerURI.equals(impl.issuerURI);
    }

    @Override
    public int hashCode() {
      int result = name.hashCode();
      result = 31 * result + fullName.hashCode();
      result = 31 * result + email.hashCode();
      result = 31 * result + id.hashCode();
      result = 31 * result + (status != null ? status.hashCode() : 0);
      result = 31 * result + issuerURI.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "Agent " + id + " ('" + name + '\'' +
              ", status=" + status +
              ", full='" + fullName + '\'' +
              ", email='" + email + '\'' +
              ", issuer=" + issuerURI +
              ')';
    }

  }

  interface Modifier {

    @Nonnull
    Modifier withFullName(@Nonnull String newName);

    @Nonnull
    Modifier withEmail(@Nonnull String newEmail);

    @Nonnull
    Modifier withIssuerURL(@Nonnull URI newIssuerURI);

    @Nonnull
    Modifier optionallyFullName(@Nullable String fullName);

    @Nonnull
    Modifier optionallyEmail(@Nullable String email);

    @Nonnull
    AgentData build();

    @NotThreadSafe
    final class Impl implements Modifier {

      private final AgentId id;
      private final String status;
      private String fullName, email, name;
      private URI issuerURI;

      private Impl(@Nonnull final AgentData agent) {
        this.id = agent.getId();
        this.status = agent.getStatus().orElse(null);
        this.name = agent.getName();
        this.fullName = agent.getFullName();
        this.email = agent.getEmail();
        this.issuerURI = agent.getIssuerURI();
      }

      @Nonnull
      @Override
      public Modifier withFullName(@Nonnull final String newFullName) {
        this.fullName = Validate.notEmpty(newFullName, "Missing full name");
        return this;
      }

      @Nonnull
      @Override
      public Modifier withEmail(@Nonnull final String newEmail) {
        this.email = Validate.notEmpty(newEmail, "Missing email");
        return this;
      }

      @Nonnull
      @Override
      public Modifier withIssuerURL(@Nonnull final URI newIssuerURI) {
        this.issuerURI = Validate.notNull(newIssuerURI, "Missing issuer URL");
        return this;
      }

      @Nonnull
      @Override
      public Modifier optionallyFullName(@Nullable final String newFullName) {
        return (isEmpty(newFullName) ? this : withFullName(newFullName));
      }

      @Nonnull
      @Override
      public Modifier optionallyEmail(@Nullable final String newEmail) {
        return (isEmpty(newEmail) ? this : withEmail(newEmail));
      }

      @Nonnull
      @Override
      public AgentData build() {
        return new AgentData.Impl(id, status, name, fullName, email, issuerURI);
      }

    }

  }

  final class JsonKeys {

    public static final String ID = "agent_id";
    public static final String STATUS = "agent_status";
    public static final String NAME = "name";
    public static final String FULL_NAME = "full_name";
    public static final String EMAIL = "email";
    public static final String ISSUER_URL = "issuer_url";

  }

}
