/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business.impl;

import de.denic.aim.client.Id4Me;
import de.denic.aim.client.business.Identity;
import de.denic.aim.client.http.AimClient;
import de.denic.aim.client.http.IdentityAuthorisation;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;
import java.net.URL;
import java.time.ZonedDateTime;

@Immutable
public final class IdentityImpl implements Identity {

  private final Id4Me identifier;
  private final String subject;
  private final URL credentialSetupURL;
  private final ZonedDateTime credentialSetupExpirationTS;
  private final AimClient aimClient;

  public IdentityImpl(@Nonnull final Id4Me identifier,
                      @Nonnull final String subject,
                      @Nonnull final URL credentialSetupURL,
                      @Nonnull final ZonedDateTime credentialSetupExpirationTS,
                      @Nonnull final AimClient aimClient) {
    this.identifier = Validate.notNull(identifier, "Missing identifier");
    this.subject = Validate.notEmpty(subject, "Missing subject");
    this.credentialSetupURL = Validate.notNull(credentialSetupURL, "Missing credential setup URI");
    this.credentialSetupExpirationTS = Validate.notNull(credentialSetupExpirationTS, "Missing credentials setup expiration TS");
    this.aimClient = Validate.notNull(aimClient, "Missing AIM Client component");
  }

  public IdentityImpl(@Nonnull final IdentityAuthorisation identityAuthorisation,
                      @Nonnull final AimClient aimClient) {
    this(Validate.notNull(identityAuthorisation, "Missing identity authorisation").getIdentifier(),
            identityAuthorisation.getSubject(),
            identityAuthorisation.getCredentialsInitURL(),
            identityAuthorisation.getCredentialsInitExpirationTS(),
            aimClient);
  }

  @Nonnull
  @Override
  public Id4Me getIdentifier() {
    return identifier;
  }

  @Nonnull
  @Override
  public @NotEmpty String getSubject() {
    return subject;
  }

  @Nonnull
  @Override
  public URL getCredentialSetupURL() {
    return credentialSetupURL;
  }

  @Nonnull
  @Override
  public ZonedDateTime getCredentialSetupExpirationTS() {
    return credentialSetupExpirationTS;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final IdentityImpl identity = (IdentityImpl) o;

    if (!identifier.equals(identity.identifier))
      return false;
    if (!subject.equals(identity.subject))
      return false;
    if (!credentialSetupURL.equals(identity.credentialSetupURL))
      return false;
    return credentialSetupExpirationTS.equals(identity.credentialSetupExpirationTS);
  }

  @Override
  public int hashCode() {
    int result = identifier.hashCode();
    result = 31 * result + subject.hashCode();
    result = 31 * result + credentialSetupURL.hashCode();
    result = 31 * result + credentialSetupExpirationTS.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Identity '" + identifier + "' (" +
            "subject='" + subject + '\'' +
            ", credentialSetupURI=" + credentialSetupURL +
            ", credentialSetupExpirationTS=" + credentialSetupExpirationTS +
            ')';
  }

}
