/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import de.denic.aim.client.AgentData;
import de.denic.aim.client.AgentId;
import de.denic.aim.client.Id4Me;

import javax.annotation.Nonnull;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.util.Locale;
import java.util.Optional;

public interface AimClient {

  @Nonnull
  AgentData registerAgentWith(@NotEmpty String name,
                              @NotEmpty String fullName,
                              @Nonnull @Email String email,
                              @Nonnull URI issuerURI) throws AimClientException;

  @Nonnull
  Optional<? extends AgentData> loadDataOfAgentWith(@Nonnull AgentId agentId) throws AimClientException;

  @Nonnull
  AgentData updateAgentTo(@Nonnull AgentData modifiedAgent) throws AimClientException;

  @Nonnull
  AuthorisationChallenge issueAuthorisationWith(@Nonnull Id4Me identifier,
                                                @Nonnull Locale locale) throws AimClientException;

  @Nonnull
  Optional<AuthorisationChallenge> loadIdentityAuthorisationWith(@Nonnull AuthzID identAuthzId) throws AimClientException;

  @Nonnull
  IdentityAuthorisation verifyDnsSetupOfIdentityAuthorisation(@Nonnull AuthzID identAuthzId) throws AimClientException;

  @Nonnull
  Optional<Identity> loadIdentityBySubject(@NotEmpty String subject) throws AimClientException;

  @Nonnull
  Optional<Identity> loadIdentityById(@Nonnull Id4Me identifier) throws AimClientException;

  @Nonnull
  PasswordSetup triggerPasswordSetupOf(@NotEmpty String subject) throws AimClientException;

  boolean deletedIdentityFromSubject(@Nonnull String subject,
                                     @Nonnull Id4Me identifier) throws AimClientException;

}
