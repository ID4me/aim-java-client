/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business.impl;

import com.nimbusds.jose.jwk.JWK;
import de.denic.aim.client.AgentId;
import de.denic.aim.client.Id4Me;
import de.denic.aim.client.business.Agent;
import de.denic.aim.client.business.DnsSetupSPI;
import de.denic.aim.client.business.Identity;
import de.denic.aim.client.business.IdentityAuthorisation;
import de.denic.aim.client.http.AimClient;
import de.denic.aim.client.http.AimClientException;
import de.denic.aim.client.http.AuthorisationChallenge;
import de.denic.aim.client.http.impl.AimClientMicronautBasedImpl;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.RxHttpClient;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.net.URL;
import java.util.Locale;
import java.util.Optional;
import java.util.ServiceLoader;

import static java.util.Comparator.reverseOrder;

@Immutable
public final class AgentImpl implements Agent {

  private static final Logger LOG = LoggerFactory.getLogger(AgentImpl.class);

  private final AimClient aimClient;
  private final HttpClient httpClient;
  private final URL aimServerURL;
  private final AgentId agentId;
  private final JWK keyPair;

  public AgentImpl(@Nonnull final URL aimServerURL,
                   @Nonnull final JWK keyPair) {
    this.aimServerURL = Validate.notNull(aimServerURL, "Missing AIM server URL");
    this.httpClient = RxHttpClient.create(aimServerURL);
    this.aimClient = new AimClientMicronautBasedImpl(aimServerURL, keyPair, httpClient);
    this.keyPair = Validate.notNull(keyPair, "Missing key pair");
    this.agentId = new AgentId(Validate.notEmpty(keyPair.getKeyID(), "Provided Key Pair misses ID"));
  }

  @Nonnull
  @Override
  public IdentityAuthorisation issueAuthorisationWith(@Nonnull final Id4Me identifier,
                                                      @Nonnull final Locale locale) throws AimClientException {
    return new IdentityAuthorisationImpl(aimClient.issueAuthorisationWith(identifier, locale));
  }

  @Override
  public void close() {
    httpClient.close();
  }

  @Override
  public String toString() {
    return "Agent '" + agentId + "' at " + aimServerURL;
  }

  @Immutable
  private final class IdentityAuthorisationImpl implements IdentityAuthorisation, DnsSetupSPI.DnsSetupVerificationCallback {

    private final AuthorisationChallenge data;

    private IdentityAuthorisationImpl(@Nonnull final AuthorisationChallenge data) {
      this.data = Validate.notNull(data, "Missing Identity Authorisation data");
    }

    @Nonnull
    @Override
    public IdentityAuthorisation reload() throws AimClientException {
      return new IdentityAuthorisationImpl(
              aimClient.loadIdentityAuthorisationWith(data.getId())
                      .orElseThrow(() -> new IllegalStateException(data + " has gone")));
    }

    @Nonnull
    @Override
    public Optional<DnsSetup> initiateDnsSetup() {
      return ServiceLoader.load(DnsSetupSPI.class).stream()
              .map(ServiceLoader.Provider::get)
              .filter(dnsSetupSPI -> dnsSetupSPI.accountableFor(data))
              .sorted(reverseOrder())
              .peek(dnsSetupSPI -> LOG.debug("Found DNS Setup SPI accountable for '{}': {}",
                      data.getIdentifier(), dnsSetupSPI))
              .findFirst()
              .map(dnsSetupSPI -> dnsSetupSPI.setup(data, keyPair, this));
    }

    @Nonnull
    @Override
    public Identity verifyDnsSetup() throws AimClientException {
      return new IdentityImpl(aimClient.verifyDnsSetupOfIdentityAuthorisation(data.getId()),
              aimClient);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final IdentityAuthorisationImpl that = (IdentityAuthorisationImpl) o;

      return data.equals(that.data);
    }

    @Override
    public int hashCode() {
      return data.hashCode();
    }

    @Override
    public String toString() {
      return data.toString();
    }

  }

//  @Immutable
//  private final class IdentityImpl implements Identity {
//
//    private final Id4Me identifier;
//    private final String subject;
//
//    private IdentityImpl(@Nonnull final Id4Me identifier, @Nonnull final String subject) {
//      this.identifier = Validate.notNull(identifier, "Missing identifier");
//      this.subject = Validate.notEmpty(subject, "Missing subject");
//    }
//
//    private IdentityImpl(@Nonnull final de.denic.aim.client.http.IdentityAuthorisation identAuthz) {
//      this(Validate.notNull(identAuthz, "Missing Identity Authorisation").getIdentifier(),
//              identAuthz.getSubject());
//    }
//
//    private IdentityImpl(@Nonnull final de.denic.aim.client.http.Identity identity) {
//      this(Validate.notNull(identity, "Missing Identity").getIdentifier(), identity.getSubject());
//    }
//
//    @Nonnull
//    @Override
//    public Id4Me getIdentifier() {
//      return identifier;
//    }
//
//    @Override
//    public @NotEmpty String getSubject() {
//      return subject;
//    }
//
//    @Nonnull
//    @Override
//    public Identity reload() {
//      return new IdentityImpl(aimClient.loadIdentityById(identifier)
//              .orElseThrow(() -> new IllegalStateException(this + " has gone")));
//    }
//
//    @Nonnull
//    @Override
//    public PasswordSetup triggerPasswordSetup() {
//      return aimClient.triggerPasswordSetupOf(subject);
//    }
//
//    @Override
//    public boolean equals(final Object o) {
//      if (this == o) return true;
//      if (o == null || getClass() != o.getClass()) return false;
//
//      final IdentityImpl identity = (IdentityImpl) o;
//
//      if (!identifier.equals(identity.identifier)) return false;
//      return subject.equals(identity.subject);
//    }
//
//    @Override
//    public int hashCode() {
//      int result = identifier.hashCode();
//      result = 31 * result + subject.hashCode();
//      return result;
//    }
//
//    @Override
//    public String toString() {
//      return "Identity " + identifier +
//              " (subject '" + subject + "')";
//    }
//
//  }

}
