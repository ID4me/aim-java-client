/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.util.Base64URL;
import de.denic.aim.client.Id4Me;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.aim.client.http.Challenge.Util.*;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.util.Optional.ofNullable;

@JsonDeserialize(as = Challenge.Impl.class)
public interface Challenge {

  @Nonnull
  Id4Me getIdentifier();

  @NotEmpty
  String getDNSChallengeToken();

  @NotEmpty
  default String computeACMEChallengeValueWith(@Nonnull final JWK publicJWK) {
    final String keyAuthorisation = computeACMEKeyAuthorisationWith(publicJWK);
    return Base64URL.encode(SHA_256_DIGEST.digest(keyAuthorisation.getBytes(US_ASCII))).toString();
  }

  @NotEmpty
  default String computeACMEKeyAuthorisationWith(@Nonnull final JWK publicJWK) {
    return computeACMEKeyAuthorisationWith(getDNSChallengeToken(), publicJWK);
  }

  @NotEmpty
  static String computeACMEChallengeValueWith(@NotEmpty final String dnsChallengeToken,
                                              @Nonnull final JWK publicJWK) {
    final String keyAuthorisation = computeACMEKeyAuthorisationWith(dnsChallengeToken, publicJWK);
    return Base64URL.encode(SHA_256_DIGEST.digest(keyAuthorisation.getBytes(US_ASCII))).toString();
  }

  @NotEmpty
  static String computeACMEKeyAuthorisationWith(@NotEmpty final String dnsChallengeToken,
                                                @Nonnull final JWK publicJWK) {
    final Base64URL base64UrlThumbprintOfAccountKey;
    try {
      base64UrlThumbprintOfAccountKey = Validate.notNull(publicJWK, "Missing key").
              computeThumbprint(SHA_256_DIGEST.getAlgorithm());
    } catch (final JOSEException e) {
      throw new RuntimeException("Internal error", e);
    }

    return Validate.notEmpty(dnsChallengeToken, "Missing DNS challenge token")
            + "."
            + base64UrlThumbprintOfAccountKey.toString();
  }

  @Nonnull
  Optional<ZonedDateTime> getValidationTS();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @JsonPropertyOrder({IDENTIFIER_KEY, DNS_CHALLENGE_TOKEN_KEY, VALIDATION_TS_KEY})
  @Immutable
  abstract class Impl implements Challenge {

    private final Id4Me identifier;
    private final String dnsChallengeToken;
    private final ZonedDateTime validationTimestamp;

    public Impl(@NotNull final Id4Me identifier,
                @NotEmpty final String dnsChallengeToken,
                @Nullable final ZonedDateTime validationTS) {
      this.identifier = Validate.notNull(identifier, "Missing identifier");
      this.dnsChallengeToken = Validate.notEmpty(dnsChallengeToken, "Missing DNS challenge token");
      this.validationTimestamp = validationTS;
    }

    @JsonGetter(IDENTIFIER_KEY)
    @Nonnull
    @Override
    public final Id4Me getIdentifier() {
      return identifier;
    }

    @JsonGetter(DNS_CHALLENGE_TOKEN_KEY)
    @Override
    public final @NotEmpty String getDNSChallengeToken() {
      return dnsChallengeToken;
    }

    @JsonIgnore
    @Override
    @Nonnull
    public final Optional<ZonedDateTime> getValidationTS() {
      return ofNullable(validationTimestamp);
    }

    @JsonGetter(VALIDATION_TS_KEY)
    @Nullable
    public final String getValidationTSAsJSON() {
      return (validationTimestamp == null ? null : AuthorisationChallenge.OPTIONAL_FRACTION_OF_SECONDS_FORMATTER.format(validationTimestamp));
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!identifier.equals(impl.identifier))
        return false;
      if (!dnsChallengeToken.equals(impl.dnsChallengeToken))
        return false;
      return validationTimestamp != null ? validationTimestamp.equals(impl.validationTimestamp) : impl.validationTimestamp == null;
    }

    @Override
    public int hashCode() {
      int result = identifier.hashCode();
      result = 31 * result + dnsChallengeToken.hashCode();
      result = 31 * result + (validationTimestamp != null ? validationTimestamp.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "'" + identifier + '\'' +
              ", token '" + dnsChallengeToken + '\'' +
              ", valid. TS '" + validationTimestamp + '\'';
    }

  }

  final class Util {

    static final String IDENTIFIER_KEY = "identifier";
    static final String VALIDATION_TS_KEY = "validation_ts";
    static final String DNS_CHALLENGE_TOKEN_KEY = "token";
    static final String SHA_256_NAME = "SHA-256";
    static final MessageDigest SHA_256_DIGEST;

    static {
      try {
        SHA_256_DIGEST = MessageDigest.getInstance(SHA_256_NAME);
      } catch (NoSuchAlgorithmException e) {
        throw new ExceptionInInitializerError(e);
      }
    }

  }

}
