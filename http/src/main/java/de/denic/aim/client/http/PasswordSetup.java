/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.http;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.time.Instant.ofEpochSecond;
import static java.time.ZonedDateTime.ofInstant;

@JsonDeserialize(as = PasswordSetup.Impl.class)
public interface PasswordSetup {

  @Nonnull
  URI getUri();

  @Nonnull
  ZonedDateTime getExpiration();

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  final class Impl implements PasswordSetup {

    private static final ZoneId Z_ZONE = ZoneId.of("Z");

    private final URI uri;
    private final ZonedDateTime expiration;

    public Impl(@Nonnull final URI uri,
                @Nonnull final ZonedDateTime expiration) {
      this.uri = Validate.notNull(uri, "Missing URI");
      this.expiration = Validate.notNull(expiration, "Missing expiration date");
    }

    public Impl(@JsonSetter("url") @Nonnull final URI uri,
                @JsonSetter("reset_expire") @Nonnull final Long expirationEpoch) {
      this(uri, ofInstant(ofEpochSecond(expirationEpoch), Z_ZONE));
    }

    @Nonnull
    @Override
    public URI getUri() {
      return uri;
    }

    @Nonnull
    @Override
    public ZonedDateTime getExpiration() {
      return expiration;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      final Impl impl = (Impl) o;

      if (!uri.equals(impl.uri)) return false;
      return expiration.equals(impl.expiration);
    }

    @Override
    public int hashCode() {
      int result = uri.hashCode();
      result = 31 * result + expiration.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "Password setup URI " + uri +
              " (expires " + expiration + ')';
    }

  }

}
