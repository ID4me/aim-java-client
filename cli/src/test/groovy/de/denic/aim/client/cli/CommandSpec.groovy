/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli

import com.nimbusds.jose.jwk.RSAKey
import io.micronaut.context.ApplicationContext
import org.apache.commons.io.output.TeeOutputStream
import spock.lang.Ignore
import spock.lang.Specification

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPublicKey

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static Command.DEFAULT_JWK_FILE
import static io.micronaut.configuration.picocli.PicocliRunner.run
import static io.micronaut.context.env.Environment.CLI
import static io.micronaut.context.env.Environment.TEST
import static java.nio.charset.StandardCharsets.UTF_8
import static java.util.UUID.randomUUID
import static org.apache.commons.lang3.SystemUtils.userDir

class CommandSpec extends Specification {

  private static final boolean FLUSH = true

  private static PrintStream originalSystemOut
  private static ByteArrayOutputStream copyOfSysOutStream

  def setupSpec() {
    System.setProperty('test-case', 'yes')
    createNewKeyPairFileIfNecessary()
  }

  def cleanupSpec() {
    System.clearProperty('test-case')
  }

  def setup() {
    copyOfSysOutStream = new ByteArrayOutputStream()
    def teeingSystemOut = new PrintStream(new TeeOutputStream(System.out, copyOfSysOutStream), FLUSH, UTF_8)
    originalSystemOut = System.out
    System.setOut(teeingSystemOut)
  }

  def cleanup() {
    System.setOut(originalSystemOut)
  }

  def 'Asking for Help'() {
    when:
    ApplicationContext.run(CLI, TEST).withCloseable { context ->
      run(Command.class, context, '--help')
    }
    def sysOut = copyOfSysOutStream.toString(UTF_8)
    //System.err.println("SYSOUT: $sysOut"'")

    then:
    sysOut.contains('Usage: java -jar aim.jar')
    sysOut.contains('agent-init, ai')
    sysOut.contains('agent-read, ar')
    sysOut.contains('authz-issue, zi')
    sysOut.contains('authz-verify, zv')
//    sysOut.contains('agent-update, au')
  }

  def 'Read unknown Agent'() {
    when:
    def unknownAgentID = randomUUID().toString()
    ApplicationContext.run(CLI, TEST).withCloseable { context ->
      run(Command.class, context, 'agent-read', '--agent', unknownAgentID)
    }
    def sysOut = copyOfSysOutStream.toString(UTF_8)
    System.err.println("SYSOUT: $sysOut")
    def lookupIsForbidden = sysOut.contains("Lookup of Agent '${unknownAgentID}' failed. Response status 'FORBIDDEN'")
    def loadingDataOfAgent = sysOut.contains("Loading data of Agent '${unknownAgentID}'")

    then:
    assert loadingDataOfAgent || sysOut.contains("No configuration file '${userDir}/agent-config.json' available")
    assert lookupIsForbidden || sysOut.contains('Missing mandatory agent config file!')
  }

  def 'Passing malformed AIM server URL argument when initialising Agent'() {
    when:
    ApplicationContext.run(CLI, TEST).withCloseable { context ->
      run(Command.class, context, 'agent-init', '--url', 'htp://id.denic.de', '--email', 'mail@test.bar',
              '--fullName', 'Test full name', '--name', 'Test name', '--issuerURL', 'http://some.issuer.url')
    }
    def sysOut = copyOfSysOutStream.toString(UTF_8)
    System.err.println("SYSOUT: $sysOut")

    then:
    sysOut.contains("Misconfigured AIM server URI 'htp://id.denic.de': unknown protocol: htp")
  }

  def 'Passing unreachable AIM server URL argument when initialising Agent'() {
    when:
    ApplicationContext.run(CLI, TEST).withCloseable { context ->
      run(Command.class, context, 'agent-init', '--url', 'http://foo.bar', '--email', 'mail@test.bar',
              '--fullName', 'Test full name', '--name', 'Test name', '--issuerURL', 'http://some.issuer.url')
    }
    def sysOut = copyOfSysOutStream.toString(UTF_8)
    System.err.println("SYSOUT: $sysOut")

    then:
    sysOut.contains('Connect Error: foo.bar')
  }

  @Ignore('Not implemented yet')
  def 'Modifying data of an unknown Agent'() {
    when:
    def unknownAgentID = randomUUID().toString()
    ApplicationContext.run(CLI, TEST).withCloseable { context ->
      run(Command.class, context, 'agent-update', '--agent', unknownAgentID, '--fullName', "Full agent's name")
    }
    def sysOut = copyOfSysOutStream.toString(UTF_8)
    System.err.println("SYSOUT: $sysOut")

    then:
    sysOut.contains("Loading Agent of ID '${unknownAgentID}'")
    sysOut.contains("Agent of ID '${unknownAgentID}' is unknown!")
  }

  private static void createNewKeyPairFileIfNecessary() {
    if (!DEFAULT_JWK_FILE.exists()) {
      assert DEFAULT_JWK_FILE.createNewFile()
      KeyPair keyPair = KeyPairGenerator.getInstance('RSA').generateKeyPair()
      final RSAKey rsaKey = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
              .privateKey(keyPair.getPrivate())
              .keyUse(SIGNATURE)
              .build()
      DEFAULT_JWK_FILE.withWriter('UTF-8') { writer -> writer.write(rsaKey.toJSONString()) }
      println "Written test JWT to ${DEFAULT_JWK_FILE}"
    }
  }

}
