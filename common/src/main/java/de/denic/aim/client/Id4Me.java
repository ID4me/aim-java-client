/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static java.net.IDN.USE_STD3_ASCII_RULES;
import static java.net.IDN.toASCII;

@Immutable
public final class Id4Me {

  private final String value;
  private final int hashCode;

  @JsonCreator
  public Id4Me(@Nonnull final String value) {
    this.value = Validate.notNull(value, "Missing value");
    this.hashCode = precalculateHashCode();
  }

  @JsonValue
  @Nonnull
  public String getValue() {
    return value;
  }

  @Nonnull
  public String toFullyQualifiedDnsName() {
    return toASCII((value.endsWith(".") ? value : value + "."), USE_STD3_ASCII_RULES);
  }

  @Nonnull
  public String toDnsName() {
    return toASCII((value.endsWith(".") ? value.substring(0, value.length() - 1) : value), USE_STD3_ASCII_RULES);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final Id4Me that = (Id4Me) o;

    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  private int precalculateHashCode() {
    return value.hashCode();
  }

  @Override
  public String toString() {
    return value;
  }
}
