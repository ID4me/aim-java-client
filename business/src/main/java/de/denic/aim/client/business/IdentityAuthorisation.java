/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.business;

import de.denic.aim.client.http.AimClientException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.Optional;

public interface IdentityAuthorisation {

  @Nonnull
  IdentityAuthorisation reload() throws AimClientException;

  /**
   * Delegated to an instance of {@link DnsSetupSPI} loaded via Java's {@link java.util.ServiceLoader} framework.
   */
  @Nonnull
  Optional<DnsSetup> initiateDnsSetup();

  interface DnsSetup {

    Duration MAX_TIMEOUT = Duration.ofMinutes(10L);

    /**
     * @see #pollDnsSetupPropagationAt(InetAddress, Duration)
     */
    @Nonnull
    default Optional<DnsSetup.Checked> pollDnsSetupPropagationFor(@Nonnull final Duration timeout) {
      return pollDnsSetupPropagationAt(Util.GOOGLE_RESOLVER_ADDRESS, timeout);
    }

    /**
     * @param timeout  Maximum of {@link #MAX_TIMEOUT} allowed!
     * @param resolver If missing Google resolver at {@link Util#GOOGLE_RESOLVER_ADDRESS} is applied.
     */
    @Nonnull
    Optional<DnsSetup.Checked> pollDnsSetupPropagationAt(@Nullable InetAddress resolver, @Nonnull Duration timeout);

    interface Checked {

      @Nonnull
      Identity issueDnsSetupVerification();

    }

  }

  final class Util {

    public static final InetAddress GOOGLE_RESOLVER_ADDRESS;

    static {
      try {
        GOOGLE_RESOLVER_ADDRESS = InetAddress.getByName("8.8.8.8");
      } catch (final UnknownHostException e) {
        throw new ExceptionInInitializerError(e);
      }
    }

  }

}
