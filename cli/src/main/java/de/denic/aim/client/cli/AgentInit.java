/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import de.denic.aim.client.AgentId;
import de.denic.aim.client.http.AimClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import javax.annotation.Nonnull;
import java.net.URI;

import static de.denic.aim.client.cli.Command.SUCCESS;
import static java.util.Collections.singleton;
import static java.util.UUID.randomUUID;

@Command(name = "agent-init", aliases = {"ai"}, description = "Registers/initializes a new Agent at an AIM server.")
public class AgentInit extends AbstractSubCommandBase {

  private static final Logger LOG = LoggerFactory.getLogger(AgentInit.class);
  private static final URI DEFAULT_AIM_SERVER_URI = URI.create("https://id.staging.denic.de");

  @Option(names = {"-e", "--email"},
          required = true,
          description = "Email address of the Agent.",
          paramLabel = "<EMAIL>")
  private String email;

  @Option(names = {"-f", "--fullName"},
          required = true,
          description = "Full name of the Agent.",
          paramLabel = "<FULL>")
  private String fullName;

  @Option(names = {"-i", "--issuerURL"},
          required = true,
          description = "Issuer URL of the Agent.",
          paramLabel = "<URL>")
  private URI issuerURI;

  @Option(names = {"-n", "--name"},
          required = true,
          description = "Name of the Agent to create.",
          paramLabel = "<NAME>")
  private String name;

  @Option(names = {"-u", "--url"},
          description = {"URL of target AIM server to contact.",
                  "Defaults to '${DEFAULT-VALUE}'"},
          paramLabel = "<SERVER-URL>")
  private volatile URI aimServerURI = DEFAULT_AIM_SERVER_URI;

  @Override
  protected Integer doCall() {
    final JWK keyPair = generateNewKeyPair();
    final AimClient aimClient = prepareAimClientFrom(aimServerURI, keyPair);
    final AgentId idOfNewlyRegisteredAgent = aimClient.registerAgentWith(name, fullName, email, issuerURI).getId();
    final AgentConfig configOfNewlyRegisteredAgent = new AgentConfig(idOfNewlyRegisteredAgent, aimServerURI, singleton(keyPair));
    getAgentConfig().map(AgentConfig::copyToBackupFile);
    changeConfigTo(configOfNewlyRegisteredAgent.savedTo(getAgentConfigFile()));
    return SUCCESS;
  }

  @Nonnull
  private static JWK generateNewKeyPair() {
    final ECKeyGenerator keyGenerator = new ECKeyGenerator(Curve.P_384);
    keyGenerator.keyID(randomUUID().toString());
    keyGenerator.keyUse(KeyUse.SIGNATURE);
    final ECKey result;
    try {
      result = keyGenerator.generate();
    } catch (final JOSEException e) {
      throw new RuntimeException("Internal error generating key-pair. " + e.getMessage());
    }

    LOG.info("Created new key-pair of type '{}' with ID '{}'", result.getKeyType(), result.getKeyID());
    return result;
  }

}
