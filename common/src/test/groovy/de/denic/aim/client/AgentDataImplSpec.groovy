/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import spock.lang.Specification

class AgentDataImplSpec extends Specification {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
          .registerModule(new Jdk8Module())
  private static final AgentId AGENT_ID = new AgentId('test.agent.id')
  private static final String STATUS = 'AgentStatus'
  private static final String NAME = 'AgentName'
  private static final String FULL_NAME = 'Agent Full Name'
  private static final String EMAIL = 'mail@test.agent'
  private static final URI ISSUER_URI = URI.create('https://agent.issuer.test')

  def 'Proper JSON serialization'() {
    setup:
    def toSerialize = new AgentData.Impl(agentId, status, name, fullName, email, issuerURI)

    expect:
    OBJECT_MAPPER.writerFor(AgentData).writeValueAsString(toSerialize) == expectedJSON

    where:
    agentId  | status | name | fullName  | email | issuerURI  || expectedJSON
    AGENT_ID | STATUS | NAME | FULL_NAME | EMAIL | ISSUER_URI || '{"agent_id":"test.agent.id","name":"AgentName",' +
            '"full_name":"Agent Full Name","agent_status":"AgentStatus","email":"mail@test.agent","issuer_url":"https://agent.issuer.test"}'
    AGENT_ID | null   | NAME | FULL_NAME | EMAIL | ISSUER_URI || '{"agent_id":"test.agent.id","name":"AgentName",' +
            '"full_name":"Agent Full Name","email":"mail@test.agent","issuer_url":"https://agent.issuer.test"}'
  }

  def 'Proper JSON deserialization'() {
    setup:
    def expectedDeserialized = new AgentData.Impl(agentId, status, name, fullName, email, issuerURI)

    expect:
    OBJECT_MAPPER.readerFor(AgentData).readValue(json) == expectedDeserialized

    where:
    json                                                                          || agentId  | status | name | fullName  | email | issuerURI
    '{"agent_id":"test.agent.id","agent_status":"AgentStatus","name":"AgentName","full_name":"Agent Full Name",' +
            '"email":"mail@test.agent","issuer_url":"https://agent.issuer.test"}' || AGENT_ID | STATUS | NAME | FULL_NAME | EMAIL | ISSUER_URI
    '{"agent_id":"test.agent.id","name":"AgentName","full_name":"Agent Full Name","email":"mail@test.agent",' +
            '"issuer_url":"https://agent.issuer.test"}'                           || AGENT_ID | null   | NAME | FULL_NAME | EMAIL | ISSUER_URI
  }

}
