/*^
  ===========================================================================
  DENIC ID Agent Identity Management (AIM) Client
  ===========================================================================
  Copyright (C) 2019-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.aim.client.cli;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.AsymmetricJWK;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import de.denic.aim.client.AgentId;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.net.URI;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.util.Collections.unmodifiableList;
import static java.util.Optional.*;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_EMPTY)
@JsonPropertyOrder({AgentConfig.AGENT_ID_KEY, AgentConfig.AIM_SERVER_URI_KEY, AgentConfig.KEY_PAIRS_KEY})
@Immutable
public final class AgentConfig {

  public static final String AIM_SERVER_URI_KEY = "aimServerURI";
  public static final String AGENT_ID_KEY = "agentID";
  public static final String KEY_PAIRS_KEY = "keyPairs";

  private static final Logger LOG = LoggerFactory.getLogger(AgentConfig.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new Jdk8Module());
  private static final ObjectReader CONFIG_JSON_READER = OBJECT_MAPPER.readerFor(AgentConfig.class);
  private static final ObjectWriter CONFIG_JSON_WRITER =
          OBJECT_MAPPER.writerFor(AgentConfig.class).with(new DefaultPrettyPrinter());
  private static final ObjectReader JSON_READER = OBJECT_MAPPER.reader();
  private static final File NO_SOURCE_FILE = null;
  private static final Function<JsonNode, JWK> JSON_TO_JWK_KEY_PAIR = jsonObject -> {
    try {
      return JWK.parse(jsonObject.toString());
    } catch (final ParseException e) {
      throw new IllegalArgumentException("JSON not parsable as RSA/EC key", e);
    }
  };

  private final AgentId agentId;
  private final URI optTargetAimServerURI;
  private final List<JsonNode> keyPairJSONs = new LinkedList<>();
  private final File optSourceFile;

  private AgentConfig(@Nonnull final AgentId agentId,
                      @Nullable final URI targetAimServerURI,
                      @Nullable final Collection<JsonNode> keyPairs,
                      @Nullable final File sourceFile) {
    this.agentId = Validate.notNull(agentId, "Missing Agent ID");
    this.optTargetAimServerURI = targetAimServerURI;
    if (keyPairs != null) {
      keyPairs.stream()
              .filter(Objects::nonNull)
              .filter(keyPair -> {
                if (isEmpty(keyPair.get("kid").textValue())) {
                  LOG.warn("Provided key-pair misses key ID!");
                  return false;
                }

                return true;
              })
              .forEach(this.keyPairJSONs::add);
    }
    this.optSourceFile = sourceFile;
  }

  public AgentConfig(@Nonnull final AgentId agentId,
                     @Nullable final URI targetAimServerURI,
                     @Nullable final Collection<JWK> keyPairs) {
    this(agentId, validateForPublicAndPrivateKey(keyPairs), targetAimServerURI);
  }

  public AgentConfig(@JsonProperty(AGENT_ID_KEY) @Nonnull final AgentId agentId,
                     @JsonProperty(KEY_PAIRS_KEY) @Nullable final Collection<JsonNode> keyPairJSONs,
                     @JsonProperty(AIM_SERVER_URI_KEY) @Nullable final URI targetAimServerURI) {
    this(agentId, targetAimServerURI, validateJSONForPublicAndPrivateKey(keyPairJSONs), NO_SOURCE_FILE);
  }

  @Nullable
  private static Collection<JsonNode> validateForPublicAndPrivateKey(@Nullable final Collection<JWK> keyPairs) {
    if (keyPairs == null) {
      return null;
    }

    return validateKeyStreamForPubAndPrivKeys(keyPairs.stream()
            .filter(Objects::nonNull));
  }

  @Nullable
  private static Collection<JsonNode> validateJSONForPublicAndPrivateKey(@Nullable final Collection<JsonNode> keyPairJSONs) {
    if (keyPairJSONs == null) {
      return null;
    }

    final Stream<JWK> jwkStream = keyPairJSONs.stream()
            .filter(Objects::nonNull)
            .map(JSON_TO_JWK_KEY_PAIR);
    return validateKeyStreamForPubAndPrivKeys(jwkStream);
  }

  @NotNull
  private static Collection<JsonNode> validateKeyStreamForPubAndPrivKeys(@NotNull final Stream<JWK> rsaKeyStream) {
    return rsaKeyStream
            .filter(keyPair -> {
              if (keyPair instanceof AsymmetricJWK) {
                return true;
              }

              LOG.warn("Unsupported type of key-pair '{}'", keyPair.getClass().getSimpleName());
              return false;
            })
            .filter(keyPair -> {
              try {
                return ((AsymmetricJWK) keyPair).toPrivateKey() != null;
              } catch (final JOSEException e) {
                LOG.warn("Invalid private-key of key-pair '{}', reason: {}", keyPair, e.getMessage());
                return false;
              }
            })
            .filter(keyPair -> {
              try {
                return ((AsymmetricJWK) keyPair).toPublicKey() != null;
              } catch (final JOSEException e) {
                LOG.warn("Invalid public-key of key-pair '{}', reason: {}", keyPair, e.getMessage());
                return false;
              }
            })
            .filter(keyPair -> {
              if (keyPair instanceof RSAKey || keyPair instanceof ECKey) {
                return true;
              }

              LOG.warn("Unsupported type of key-pair '{}'", keyPair.getClass().getSimpleName());
              return false;
            })
            .map(AgentConfig::jwkToJsonNode)
            .collect(toList());
  }

  private AgentConfig(@Nonnull final AgentConfig config, @Nullable final File sourceFile) {
    this(Validate.notNull(config, "Missing initial config instance").agentId,
            config.optTargetAimServerURI,
            config.keyPairJSONs,
            sourceFile);
  }

  @Nonnull
  @JsonGetter(AIM_SERVER_URI_KEY)
  public Optional<URI> getTargetAimServerURI() {
    return ofNullable(optTargetAimServerURI);
  }

  @Nonnull
  @JsonGetter(AGENT_ID_KEY)
  public AgentId getAgentID() {
    return agentId;
  }

  @Nonnull
  @JsonIgnore
  public List<JWK> getKeyPairs() {
    return listOfPubPrivJWKsFrom(keyPairJSONs);
  }

  @Nonnull
  @JsonIgnore
  public Optional<JWK> getFirstKeyPair() {
    return getKeyPairs().stream().findFirst();
  }

  @Nonnull
  @JsonGetter(KEY_PAIRS_KEY)
  public List<JsonNode> getKeyPairsAsJSON() {
    return unmodifiableList(keyPairJSONs);
  }

  @Nonnull
  private AgentConfig savedToSourceFile() {
    if (optSourceFile != null) {
      try (final OutputStream fileOutputStream = new FileOutputStream(optSourceFile)) {
        savedTo(fileOutputStream);
        LOG.info("Saved configuration to file '{}'", optSourceFile);
      } catch (final IOException e) {
        LOG.warn("Saving configuration to file '{}' failed, reason: {}", optSourceFile, e.getMessage());
      }
    }
    return this;
  }

  @Nonnull
  public AgentConfig copyToBackupFile() {
    if (optSourceFile == null)
      return this;

    final String sourceFileName = optSourceFile.getName();
    final String backupFileName = sourceFileName.substring(0, sourceFileName.lastIndexOf('.')) + "_" + agentId +
            sourceFileName.substring(sourceFileName.lastIndexOf('.'));
    final File backupFile = new File(optSourceFile.getParent(), backupFileName);
    return savedTo(backupFile);
  }

  /**
   * Mainly for testing purposes.
   */
  @Nonnull
  public AgentConfig savedTo(@Nonnull final File targetFile) {
    return new AgentConfig(agentId, optTargetAimServerURI, keyPairJSONs, Validate.notNull(targetFile, "Missing target file")).savedToSourceFile();
  }

  /**
   * Mainly for testing purposes.
   */
  void savedTo(@Nonnull final OutputStream outputStream) throws IOException {
    CONFIG_JSON_WRITER.writeValue(Validate.notNull(outputStream, "Missing target stream"), this);
  }

  @Nonnull
  private AgentConfig loadedFrom(@Nonnull final File sourceFile) {
    return new AgentConfig(this, sourceFile);
  }

  public boolean alreadyLoadedFrom(@Nullable final File sourceFile) {
    return (optSourceFile != null && optSourceFile.equals(sourceFile));
  }

  @Nonnull
  public Modifiable modify() {
    return new Modifiable(agentId, optTargetAimServerURI, keyPairJSONs, optSourceFile);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final AgentConfig config = (AgentConfig) o;

    if (agentId != null ? !agentId.equals(config.agentId) : config.agentId != null)
      return false;
    if (!optTargetAimServerURI.equals(config.optTargetAimServerURI))
      return false;
    if (!keyPairJSONs.equals(config.keyPairJSONs))
      return false;
    return optSourceFile != null ? optSourceFile.equals(config.optSourceFile) : config.optSourceFile == null;
  }

  @Override
  public int hashCode() {
    int result = agentId != null ? agentId.hashCode() : 0;
    result = 31 * result + optTargetAimServerURI.hashCode();
    result = 31 * result + keyPairJSONs.hashCode();
    result = 31 * result + (optSourceFile != null ? optSourceFile.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AIM server=" + optTargetAimServerURI +
            ", agent ID=" + agentId +
            ", pub. keys=" + keyPairJSONs +
            ", source=" + optSourceFile;
  }

  @Nonnull
  public static Optional<AgentConfig> loadFromFile(@Nonnull final File sourceFile) {
    try (final InputStream fileInputStream = new FileInputStream(Validate.notNull(sourceFile, "Missing source file"))) {
      LOG.info("Loading config from '{}'", sourceFile);
      return of(loadFrom(fileInputStream).loadedFrom(sourceFile));
    } catch (final FileNotFoundException e) {
      LOG.debug("No configuration file '{}' available", sourceFile);
    } catch (final IOException e) {
      LOG.warn("Loading configuration from file '{}' failed, reason: {}", sourceFile, e.getMessage());
    }
    return empty();
  }

  private static List<JWK> listOfPubPrivJWKsFrom(final Collection<JsonNode> keyPairJSONs) {
    return keyPairJSONs.stream()
            .map(JSON_TO_JWK_KEY_PAIR)
            .collect(toList());
  }

  /**
   * Mainly for testing purposes.
   */
  @Nonnull
  static AgentConfig loadFrom(@Nonnull final InputStream inputStream) throws IOException {
    return CONFIG_JSON_READER.readValue(Validate.notNull(inputStream, "Missing source stream"));
  }

  @NotThreadSafe
  public static final class Modifiable {

    private final AgentId agentID;
    private final List<JsonNode> keyPairJSONs = new LinkedList<>();
    private final File sourceFile;
    private URI aimServerURI;

    private Modifiable(@Nonnull final AgentId agentId,
                       @Nullable final URI optTargetAimServerURI,
                       @Nullable final List<JsonNode> keyPairJSONs,
                       @Nullable final File optSourceFile) {
      this.agentID = Validate.notNull(agentId, "Missing Agent ID");
      this.aimServerURI = optTargetAimServerURI;
      if (keyPairJSONs != null) {
        this.keyPairJSONs.addAll(keyPairJSONs);
      }
      this.sourceFile = optSourceFile;
    }

    @Nonnull
    public Modifiable withTargetAimServerURI(@Nonnull final URI aimServerURI) {
      this.aimServerURI = Validate.notNull(aimServerURI, "Missing AIM server URI");
      return this;
    }

    @Nonnull
    public Modifiable addKeyPair(@Nonnull final JWK keyPair) {
      this.keyPairJSONs.add(jwkToJsonNode(keyPair));
      return this;
    }

    @Nonnull
    public AgentConfig finished() {
      return new AgentConfig(agentID, aimServerURI, keyPairJSONs, sourceFile).savedToSourceFile();
    }

  }

  @Nonnull
  private static JsonNode jwkToJsonNode(@Nonnull final JWK keyPair) {
    final JsonNode jsonNodeOfKeyPair;
    try {
      jsonNodeOfKeyPair = JSON_READER.readTree(Validate.notNull(keyPair, "Missing key pair").toJSONString());
    } catch (final JsonProcessingException e) {
      throw new RuntimeException("Internal error parsing JWK key pair", e);
    }
    return jsonNodeOfKeyPair;
  }

}
